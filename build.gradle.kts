import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.io.*
import javax.json.Json

buildscript {
    val kotlin_version = "1.3.31"

    repositories {
        mavenLocal()
        mavenCentral()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version")
        classpath("org.glassfish:javax.json:1.1.4")

        classpath("org.junit.platform:junit-platform-gradle-plugin:1.1.0")
        classpath("org.junit.platform:junit-platform-launcher:1.1.0")
    }
}

plugins {
    application
    java
    id("org.jetbrains.kotlin.jvm").version("1.3.31")
}

val platform = with(org.gradle.internal.os.OperatingSystem.current()) {
    when {
        isWindows -> "win"
        isLinux -> "linux"
        isMacOsX -> "mac"
        else -> "unknown"
    }
}

val compileKotlin: KotlinCompile by tasks
val compileTestKotlin: KotlinCompile by tasks

compileKotlin.kotlinOptions.jvmTarget = "1.8"
compileTestKotlin.kotlinOptions.jvmTarget = "1.8"

val fxModules = arrayListOf("base", "graphics", "controls", "swing")

tasks.withType<JavaCompile>().all {
    doFirst {
        options.compilerArgs = mutableListOf(
                "--module-path", classpath.asPath,
                "--add-modules", fxModules.joinToString(separator = ",") { "javafx.$it" })
    }
}

application {
    tasks.withType<JavaExec>().all {
        doFirst {
            jvmArgs = mutableListOf(
                    "--module-path", classpath.asPath,
                    "--add-modules", fxModules.joinToString(separator = ",") { "javafx.$it" })
        }
    }
    mainClassName = "example.andrew.Main"
}

repositories {
    mavenLocal()
    mavenCentral()
}

val kotlin_version = "1.3.31"
val tornadofx_version = "1.7.17"
val junit_version = "5.1.0"

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
    implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlin_version")
    implementation("no.tornado:tornadofx:$tornadofx_version")
    for (module in fxModules) {
        implementation("org.openjfx:javafx-$module:11:$platform")
    }
    
    implementation("com.github.salomonbrys.kotson:kotson:2.5.0")
    implementation("org.glassfish:javax.json:1.1.4")

    testImplementation("org.junit.jupiter:junit-jupiter-api:$junit_version")
    testRuntime("org.junit.jupiter:junit-jupiter-engine:$junit_version")
}

tasks.withType<Test>().all {
    useJUnitPlatform {
        includeEngines("junit-jupiter")
    }
}

fun String.toRelativePath() = this.replace(".", "/") + ".class"

fun getDefaultClasses(): ArrayList<String> {
    val list = arrayListOf<String>()
    try {
        val reader = Json.createReader(FileReader("sdk.config.json"))
        val obj = reader.readObject()
        val toolGroups = obj.getJsonArray("toolGroups")
        for (group in toolGroups) {
            list.add(group.asJsonObject().getString("class").toRelativePath())
            if (group.asJsonObject().containsKey("ext_points")) {
                val extArray = group.asJsonObject().getJsonArray("ext_points")
                for (ext in extArray) {
                    list.add(ext.asJsonObject().getString("class").toRelativePath())
                }
            }
        }
        return list
    } catch(exc: Exception) {
        println("Can't read sdk.config.json")
        return arrayListOf()
    }
}

tasks {
    register<Jar>("sdk-jar") {
        archiveName = "ootpisp-sdk.jar"
        from("build/classes/kotlin/main")
        include("example/andrew/sdk/*")
        for (className in getDefaultClasses()) {
            include(className)
        }
    }
    register<Exec>("sdk") {
        dependsOn("sdk-jar")
        commandLine("mvn", "install:install-file", "-Dfile=build/libs/ootpisp-sdk.jar",
                "-DgroupId=example.andrew", "-DartifactId=ootpisp-sdk", "-Dversion=1.0.0", "-Dpackaging=jar")
    }
}

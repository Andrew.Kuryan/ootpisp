package example.andrew

import example.andrew.logic.trees.AVLTree

class AVLTreeTest : BinaryTreeTest() {

    override val name = "AVL tree"
    override val tree = AVLTree<Int, String>()

    override val buildResult =
            """			(22, d, 0)
		(20, c, 0)
			(13, h, 0)
	(11, f, 0)
		(8, g, 1)
			(7, i, 0)
(6, a, 0)
		(5, n, 0)
	(4, b, 1)
			(3, m, 0)
		(2, p, 0)
			(1, o, 0)
"""

    override val removeResult =
            """		(22, d, 0)
	(11, f, -1)
(3, m, 0)
	(2, p, 1)
		(1, o, 0)
"""
}
package example.andrew.serializing

import example.andrew.tools.serialize_group.extensions.tree_serializer.binary_serializer.BinaryTreeSerializer
import example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.custom.CustomTreeSerializer
import example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.json.JsonTreeSerializer
import example.andrew.logic.trees.BinaryTree
import example.andrew.sdk.undefinedGroup
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.util.ArrayList

class BinarySerializeTest {

    val tree = BinaryTree<Int, String>()

    val strResult =
            """		(22, d)
	(20, c)
			(13, h)
		(11, f)
			(8, g)
				(7, i)
(6, a)
		(5, n)
	(4, b)
		(3, m)
				(2, p)
			(1, o)
"""

    val strArrResult =
            """		(22, [abcdefghiklmnoprst, sdfa, af])
	(20, [asdf, asf, aa, a, a])
			(13, [ljkfdg, jfkgoir])
		(11, [aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa, fldbs, sf])
			(8, [sadfa, afsa, bv])
				(7, [qwe, dfgg])
(6, [abc, def, aadf])
		(5, [ads, af, f, asf, a, fa, f, af])
	(4, [sadfas, asdfa, adfa])
		(3, [adfa, adf, a, fa, faf, asdf, a, sdfa, sd, a, f])
				(2, [';[l,;l,;, ;efkadfld])
			(1, [12345, 67890])
"""

    @BeforeEach
    fun createTree() {
        tree.insert(6, "a")
        tree.insert(4, "b")
        tree.insert(20, "c")
        tree.insert(22, "d")
        tree.insert(11, "f")
        tree.insert(3, "m")
        tree.insert(5, "n")
        tree.insert(8, "g")
        tree.insert(13, "h")
        tree.insert(7, "i")
        tree.insert(1, "o")
        tree.insert(2, "p")
    }

    @Test
    @DisplayName("should serialize Binary tree with String values in JSON format")
    fun testJsonParse() {
        val serializer = JsonTreeSerializer(undefinedGroup)
        val bytes = serializer.serialize(tree)
        val tree = serializer.parse(bytes)
        assertEquals(strResult, tree.toString(), "Binary tree serializing is not correct")
    }

    @Test
    @DisplayName("should serialize Binary tree with ArrayList<String> values in Binary format")
    fun testBinParse() {
        val folder = File("src/test/resources/preset_values/stringarray_values")
        val files = folder.listFiles()
        files.sortBy { it.name.toInt() }
        val tree = BinaryTree<Int, ArrayList<String>>()
        for (f in files) {
            val lines = Files.readAllLines(Paths.get(f.canonicalPath))
            val arrayList = ArrayList(lines.subList(1, lines.size))
            tree.insert(lines[0].toInt(), arrayList)
        }
        val serializer = BinaryTreeSerializer(undefinedGroup)
        val bytes = serializer.serialize(tree)
        val result = serializer.parse(bytes)
        assertEquals(strArrResult, result.toString(), "Binary tree serializing is not correct")
    }

    @Test
    @DisplayName("should serialize Binary tree with String values in Custom format")
    fun testCustomParse() {
        val serializer = CustomTreeSerializer(undefinedGroup)
        val bytes = serializer.serialize(tree)
        val tree = serializer.parse(bytes)
        assertEquals(strResult, tree.toString(), "Binary tree serializing is not correct")
    }
}

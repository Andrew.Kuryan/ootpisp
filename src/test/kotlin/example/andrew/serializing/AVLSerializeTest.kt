package example.andrew.serializing

import example.andrew.tools.serialize_group.extensions.tree_serializer.binary_serializer.BinaryTreeSerializer
import example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.custom.CustomTreeSerializer
import example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.json.JsonTreeSerializer
import example.andrew.logic.trees.AVLTree
import example.andrew.sdk.undefinedGroup
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.util.ArrayList

class AVLSerializeTest {

    val tree = AVLTree<Int, String>()

    val strResult =
            """			(22, d, 0)
		(20, c, 0)
			(13, h, 0)
	(11, f, 0)
		(8, g, 1)
			(7, i, 0)
(6, a, 0)
		(5, n, 0)
	(4, b, 1)
			(3, m, 0)
		(2, p, 0)
			(1, o, 0)
"""

    val strArrResult =
            """			(22, [abcdefghiklmnoprst, sdfa, af], 0)
		(20, [asdf, asf, aa, a, a], 0)
			(13, [ljkfdg, jfkgoir], 0)
	(11, [aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa, fldbs, sf], 0)
		(8, [sadfa, afsa, bv], 1)
			(7, [qwe, dfgg], 0)
(6, [abc, def, aadf], 0)
		(5, [ads, af, f, asf, a, fa, f, af], 0)
	(4, [sadfas, asdfa, adfa], 1)
			(3, [adfa, adf, a, fa, faf, asdf, a, sdfa, sd, a, f], 0)
		(2, [';[l,;l,;, ;efkadfld], 0)
			(1, [12345, 67890], 0)
"""

    @BeforeEach
    fun createTree() {
        tree.insert(6, "a")
        tree.insert(4, "b")
        tree.insert(20, "c")
        tree.insert(22, "d")
        tree.insert(11, "f")
        tree.insert(3, "m")
        tree.insert(5, "n")
        tree.insert(8, "g")
        tree.insert(13, "h")
        tree.insert(7, "i")
        tree.insert(1, "o")
        tree.insert(2, "p")
    }

    @Test
    @DisplayName("should serialize AVL tree with String values in JSON format")
    fun testJsonParse() {
        val serializer = JsonTreeSerializer(undefinedGroup)
        val bytes = serializer.serialize(tree)
        val result = serializer.parse(bytes)
        assertEquals(strResult, result.toString(), "AVL tree serializing is not correct")
    }

    @Test
    @DisplayName("should serialize AVL tree with String values in Binary format")
    fun testBinaryParse() {
        val serializer = BinaryTreeSerializer(undefinedGroup)
        val bytes = serializer.serialize(tree)
        val tree = serializer.parse(bytes)
        assertEquals(strResult, tree.toString(), "AVL tree serializing is not correct")
    }

    @Test
    @DisplayName("should serialize AVL tree with ArrayList<String> values in Custom format")
    fun testCustomParse() {
        val folder = File("src/test/resources/preset_values/stringarray_values")
        val files = folder.listFiles()
        files.sortBy { it.name.toInt() }
        val tree = AVLTree<Int, ArrayList<String>>()
        for (f in files) {
            val lines = Files.readAllLines(Paths.get(f.canonicalPath))
            val arrayList = ArrayList(lines.subList(1, lines.size))
            tree.insert(lines[0].toInt(), arrayList)
        }
        val serializer = CustomTreeSerializer(undefinedGroup)
        val bytes = serializer.serialize(tree)
        val result = serializer.parse(bytes)
        assertEquals(strArrResult, result.toString(), "AVL tree serializing is not correct")
    }
}

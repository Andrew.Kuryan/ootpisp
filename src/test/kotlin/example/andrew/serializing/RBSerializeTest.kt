package example.andrew.serializing

import example.andrew.tools.serialize_group.extensions.tree_serializer.binary_serializer.BinaryTreeSerializer
import example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.custom.CustomTreeSerializer
import example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.json.JsonTreeSerializer
import example.andrew.logic.trees.RBTree
import example.andrew.sdk.undefinedGroup
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.util.ArrayList

class RBSerializeTest {

    val tree = RBTree<Int, String>()

    val strResult =
            """		(22, d, Black)
	(20, c, Black)
			(13, h, Black)
		(11, f, Red)
			(8, g, Black)
				(7, i, Red)
(6, a, Black)
		(5, n, Black)
	(4, b, Black)
			(3, m, Black)
		(2, p, Red)
			(1, o, Black)
"""

    val strArrResult =
            """		(22, [abcdefghiklmnoprst, sdfa, af], Black)
	(20, [asdf, asf, aa, a, a], Black)
			(13, [ljkfdg, jfkgoir], Black)
		(11, [aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa, fldbs, sf], Red)
			(8, [sadfa, afsa, bv], Black)
				(7, [qwe, dfgg], Red)
(6, [abc, def, aadf], Black)
		(5, [ads, af, f, asf, a, fa, f, af], Black)
	(4, [sadfas, asdfa, adfa], Black)
			(3, [adfa, adf, a, fa, faf, asdf, a, sdfa, sd, a, f], Black)
		(2, [';[l,;l,;, ;efkadfld], Red)
			(1, [12345, 67890], Black)
"""

    @BeforeEach
    fun createTree() {
        tree.insert(6, "a")
        tree.insert(4, "b")
        tree.insert(20, "c")
        tree.insert(22, "d")
        tree.insert(11, "f")
        tree.insert(3, "m")
        tree.insert(5, "n")
        tree.insert(8, "g")
        tree.insert(13, "h")
        tree.insert(7, "i")
        tree.insert(1, "o")
        tree.insert(2, "p")
    }

    @Test
    @DisplayName("should serialize Red-black tree with ArrayList<String> values in JSON format")
    fun testJsonParse() {
        val folder = File("src/test/resources/preset_values/stringarray_values")
        val files = folder.listFiles()
        files.sortBy { it.name.toInt() }
        val tree = RBTree<Int, ArrayList<String>>()
        for (f in files) {
            val lines = Files.readAllLines(Paths.get(f.canonicalPath))
            val arrayList = ArrayList(lines.subList(1, lines.size))
            tree.insert(lines[0].toInt(), arrayList)
        }
        val serializer = JsonTreeSerializer(undefinedGroup)
        val bytes = serializer.serialize(tree)
        val result = serializer.parse(bytes)
        assertEquals(strArrResult, result.toString(), "Reb-black tree serializing is not correct")
    }

    @Test
    @DisplayName("should serialize Red-black tree with String values in binary format")
    fun testBinaryParse() {
        val serializer = BinaryTreeSerializer(undefinedGroup)
        val bytes = serializer.serialize(tree)
        val tree = serializer.parse(bytes)
        assertEquals(strResult, tree.toString(), "Red-black tree serializing is not correct")
    }

    @Test
    @DisplayName("should serialize Red-black tree with String values in custom format")
    fun testCustomParse() {
        val serializer = CustomTreeSerializer(undefinedGroup)
        val bytes = serializer.serialize(tree)
        val tree = serializer.parse(bytes)
        assertEquals(strResult, tree.toString(), "Red-black tree serializing is not correct")
    }
}

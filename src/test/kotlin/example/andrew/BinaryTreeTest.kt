package example.andrew

import example.andrew.logic.trees.BinaryTree
import example.andrew.sdk.trees.NoSuchKeyException
import example.andrew.sdk.trees.ExistingKeyException
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals

open class BinaryTreeTest {

    open val name: String = "Binary tree"
    open val tree = BinaryTree<Int, String>()

    open val buildResult =
            """		(22, d)
	(20, c)
			(13, h)
		(11, f)
			(8, g)
				(7, i)
(6, a)
		(5, n)
	(4, b)
		(3, m)
				(2, p)
			(1, o)
"""

    open val removeResult =
            """		(22, d)
	(11, f)
(3, m)
		(2, p)
	(1, o)
"""

    @BeforeEach
    fun createTree() {
        tree.insert(6, "a")
        tree.insert(4, "b")
        tree.insert(20, "c")
        tree.insert(22, "d")
        tree.insert(11, "f")
        tree.insert(3, "m")
        tree.insert(5, "n")
        tree.insert(8, "g")
        tree.insert(13, "h")
        tree.insert(7, "i")
        tree.insert(1, "o")
        tree.insert(2, "p")
    }

    @Test
    @DisplayName("should build tree correctly")
    open fun testBuild() {
        assertEquals(buildResult, tree.toString(), "$name building is not correct")
    }

    @Test
    @DisplayName("should return error if key already exist")
    open fun testInsertExist() {
        val thrown = assertThrows<ExistingKeyException> {
            tree.insert(13, "r")
        }
        assertEquals(13, thrown.key, "Insert existing key in $name is not correct")
    }

    @Test
    @DisplayName("should correctly remove some nodes from tree")
    open fun testRemove() {
        tree.remove(8)
        tree.remove(7)
        tree.remove(20)
        tree.remove(5)
        tree.remove(13)
        tree.remove(4)
        tree.remove(6)
        assertEquals(removeResult, tree.toString(), "Remove from $name is not correct")
    }

    @Test
    @DisplayName("should return error if deleting from empty tree")
    fun testRemoveEmpty() {
        tree.remove(8)
        tree.remove(7)
        tree.remove(20)
        tree.remove(5)
        tree.remove(13)
        tree.remove(4)
        tree.remove(6)
        tree.remove(3)
        tree.remove(11)
        tree.remove(2)
        tree.remove(1)
        tree.remove(22)
        val thrown = assertThrows<NoSuchKeyException> {
            tree.remove(22)
        }
        assertEquals(22, thrown.key, "Remove from empty $name is not correct")
    }

    @Test
    @DisplayName("should find keys in tree")
    open fun testFind() {
        assertEquals("c", tree.find(20).value)
        assertEquals("a", tree.find(6).value)
    }

    @Test
    @DisplayName("should return error if key not exist")
    fun testFindNotExist() {
        val thrown = assertThrows<NoSuchKeyException> {
            tree.find(15)
        }
        assertEquals(15, thrown.key)
    }
}
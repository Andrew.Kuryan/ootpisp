package example.andrew

import example.andrew.logic.trees.RBTree

class RBTreeTest : BinaryTreeTest() {

    override val name = "RB tree"
    override var tree = RBTree<Int, String>()

    override val buildResult =
            """		(22, d, Black)
	(20, c, Black)
			(13, h, Black)
		(11, f, Red)
			(8, g, Black)
				(7, i, Red)
(6, a, Black)
		(5, n, Black)
	(4, b, Black)
			(3, m, Black)
		(2, p, Red)
			(1, o, Black)
"""

    override val removeResult =
            """	(22, d, Black)
(11, f, Black)
		(3, m, Black)
	(2, p, Red)
		(1, o, Black)
"""
}
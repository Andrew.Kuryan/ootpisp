package example.andrew.sdk

import javafx.beans.property.SimpleBooleanProperty
import javafx.event.EventTarget
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

class ToolGroupTest {

    class TestToolGroup: ToolGroup(null, "Test group")
    class TestTool1: Tool(null,
            "Test tool 1",
            icoPath = "/",
            disableProperty = SimpleBooleanProperty(false)) {

        override fun action(source: EventTarget) {}
    }
    class TestTool2: Tool(null,
            "Test tool 2",
            icoPath = "/",
            disableProperty = SimpleBooleanProperty(false)) {

        override fun action(source: EventTarget) {}
    }

    lateinit var testToolGroup: TestToolGroup

    @BeforeEach
    fun initToolGroup() {
        testToolGroup = TestToolGroup()
    }

    @Test
    @DisplayName("should register tools")
    fun testRegisterTools() {
        testToolGroup.registerTool<TestTool1>()
        testToolGroup.registerTool<TestTool2>()
        assertEquals(arrayListOf(TestTool1::class, TestTool2::class), testToolGroup.availableTools)
    }

    @Test
    @DisplayName("should install tools")
    fun testInstallTools() {
        testToolGroup.registerTool<TestTool1>()
        testToolGroup.registerTool<TestTool2>()

        testToolGroup.installTool(TestTool1())
        testToolGroup.installTool(TestTool2())
        assertEquals(2, testToolGroup.tools.size)
    }

    @Test
    @DisplayName("should install tools in array")
    fun testInstallManyTools() {
        testToolGroup.registerTool<TestTool1>()
        testToolGroup.registerTool<TestTool2>()

        testToolGroup.installTools(TestTool1(), TestTool2())
        assertEquals(2, testToolGroup.tools.size)
    }

    @Test
    @DisplayName("should not install if tool not registered")
    fun testInstallUnregisterTool() {
        class TestTool3: Tool(null,
                "Test tool 3",
                icoPath = "/",
                disableProperty = SimpleBooleanProperty(false)) {

            override fun action(source: EventTarget) {}
        }

        testToolGroup.registerTool<TestTool1>()
        testToolGroup.registerTool<TestTool2>()

        testToolGroup.installTools(TestTool1(), TestTool2(), TestTool3())
        assertEquals(2, testToolGroup.tools.size)
    }
}
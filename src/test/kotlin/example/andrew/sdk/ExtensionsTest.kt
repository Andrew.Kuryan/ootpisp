package example.andrew.sdk

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

class ExtensionsTest {

    class TestExt: Expandable()
    class TestExtPoint1(override val parent: Expandable): ExtPoint
    class TestExtPoint2(override val parent: Expandable): ExtPoint

    lateinit var testExt: TestExt

    @BeforeEach
    fun initExtension() {
        testExt = TestExt()
    }

    @Test
    @DisplayName("should register extension points")
    fun testRegisterExtensions() {
        testExt.registerExtPoint<TestExtPoint1>()
        testExt.registerExtPoint<TestExtPoint2>()
        assertEquals(arrayListOf(TestExtPoint1::class, TestExtPoint2::class), testExt.availableExtensions)
    }

    @Test
    @DisplayName("should install extensions")
    fun testInstallExtensions() {
        testExt.registerExtPoint<TestExtPoint1>()
        testExt.registerExtPoint<TestExtPoint2>()

        testExt.installExtension(TestExtPoint1(testExt))
        testExt.installExtension(TestExtPoint2(testExt))
        assertEquals(2, testExt.extensions.size)
    }

    @Test
    @DisplayName("should install extensions in array")
    fun testInstallManyExtensions() {
        testExt.registerExtPoint<TestExtPoint1>()
        testExt.registerExtPoint<TestExtPoint2>()

        testExt.installExtensions(TestExtPoint1(testExt), TestExtPoint2(testExt))
        assertEquals(2, testExt.extensions.size)
    }

    @Test
    @DisplayName("should not install extension if not registered")
    fun testInstallUnregisteredExtension() {
        class TestExtPoint3(override val parent: Expandable): ExtPoint

        testExt.registerExtPoint<TestExtPoint1>()
        testExt.registerExtPoint<TestExtPoint2>()

        testExt.installExtensions(TestExtPoint1(testExt), TestExtPoint3(testExt))
        assertEquals(1, testExt.extensions.size)
    }
}
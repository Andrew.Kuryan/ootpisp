package example.andrew

import example.andrew.logic.trees.AVLTreeFactory
import example.andrew.logic.trees.BinaryTreeFactory
import example.andrew.logic.trees.RBTreeFactory
import example.andrew.sdk.ExpTarget
import example.andrew.sdk.Expandable
import example.andrew.sdk.ExtPoint
import example.andrew.sdk.OotpispApp
import example.andrew.sdk.Tool
import example.andrew.sdk.ToolGroup
import example.andrew.sdk.ToolGroupTarget
import example.andrew.sdk.findExpandable
import example.andrew.sdk.findTool
import example.andrew.sdk.findToolGroup
import example.andrew.tools.DefaultToolGroup
import example.andrew.tools.app_group.AppToolGroup
import example.andrew.ui.views.TreeViewFactory
import example.andrew.tools.serialize_group.SerializeToolGroup
import example.andrew.tools.tree_group.TreeToolGroup
import example.andrew.ui.extensions.AnyInputFactory
import example.andrew.ui.extensions.ArrayListInputFactory
import example.andrew.ui.extensions.ImageInputFactory
import example.andrew.ui.extensions.IntInputFactory
import example.andrew.ui.extensions.StringInputFactory
import example.andrew.ui.views.Editor
import example.andrew.ui.views.MainView
import example.andrew.ui.views.NodeViewFactory
import javafx.scene.Scene
import javafx.stage.Screen
import javafx.stage.Stage
import tornadofx.*
import java.io.File
import java.net.URL
import java.net.URLClassLoader
import kotlin.reflect.full.primaryConstructor
import java.util.zip.ZipEntry
import java.io.FileInputStream
import java.nio.file.Files
import java.nio.file.Paths
import java.util.zip.ZipInputStream
import java.util.ArrayList
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.isSubclassOf


class Main : App(MainView::class), OotpispApp {

    companion object {
        const val windowWidth = 1200.0
        const val windowHeight = 800.0
    }

    private lateinit var primeView: MainView

    override fun start(stage: Stage) {
        super.start(stage)
        stage.width = windowWidth
        stage.height = windowHeight
        stage.x = Screen.getPrimary().visualBounds.width / 2 - stage.width / 2
        stage.y = Screen.getPrimary().visualBounds.height / 2 - stage.height / 2
        initDefTools()
        loadPlugins()
        for ((i, toolGroup) in installedToolGroups.withIndex()) {
            if (i != 0) {
                primeView.addSeparator()
            }
            for (tool in toolGroup.tools) {
                addTool(tool)
            }
        }
    }

    private fun initDefTools() {
        installedToolGroups.addAll(arrayListOf(
            AppToolGroup(this),
            TreeToolGroup(this),
            SerializeToolGroup(this)
        ))
    }

    class DynamicClassLoader: ClassLoader() {

        fun createClass(bytes: ByteArray, name: String): KClass<*> {
            return defineClass(name, bytes, 0, bytes.size).kotlin
        }
    }

    private fun loadPlugins() {
        val pluginFolder = File("plugins")
        val plugins = pluginFolder.listFiles().map { it.name }
        val cl = URLClassLoader(plugins.map { URL("file:./plugins/$it") }.toTypedArray())
        val classNames = ArrayList<String>()
        // получение имен всех классов
        for (plugin in plugins) {
            println(plugin)
            val zip = ZipInputStream(FileInputStream("plugins/$plugin"))
            var entry: ZipEntry? = zip.nextEntry
            while (entry != null) {
                if (!entry.isDirectory && entry.name.endsWith(".class")) {
                    val className = entry.name.replace('/', '.')
                    classNames.add(className.substring(0, className.length - ".class".length))
                    println("|- ${className.substring(0, className.length - ".class".length)}")
                }
                entry = zip.nextEntry
            }
        }
        // загрузка ToolGroup
        for (className in classNames) {
            val classInst = cl.loadClass(className).kotlin
            try {
                if (classInst.isSubclassOf(ToolGroup::class)) {
                    val toolGroup = classInst.primaryConstructor?.call(this) as ToolGroup
                    installedToolGroups.add(toolGroup)
                }
            } catch (exc: kotlin.reflect.jvm.internal.KotlinReflectionInternalError) {}
        }
        installedToolGroups.add(DefaultToolGroup(this))
        // загрузка Tool's
        for (className in classNames) {
            val classInst = cl.loadClass(className).kotlin
            try {
                if (classInst.isSubclassOf(Tool::class)) {
                    val toolGroupTarget = classInst.findAnnotation<ToolGroupTarget>()?.target
                    if (toolGroupTarget != null) {
                        val toolGroup = findToolGroup(toolGroupTarget)
                        if (toolGroup?.findTool(classInst as KClass<out Tool>) == null) {
                            val tool = classInst.primaryConstructor?.call(this) as Tool
                            findToolGroup(toolGroupTarget)?.registerTool(classInst as KClass<out Tool>)
                            findToolGroup(toolGroupTarget)?.installTool(tool)
                        }
                    } else {
                        val tool = classInst.primaryConstructor?.call(this) as Tool
                        findToolGroup<DefaultToolGroup>()?.registerTool(classInst as KClass<out Tool>)
                        findToolGroup<DefaultToolGroup>()?.installTool(tool)
                    }
                }
            } catch (exc: kotlin.reflect.jvm.internal.KotlinReflectionInternalError) {}
        }
        // загрузка Extensions
        for (className in classNames) {
            val classInst = cl.loadClass(className).kotlin
            try {
                if (classInst.isSubclassOf(ExtPoint::class)) {
                    val expTarget = classInst.findAnnotation<ExpTarget>()?.target
                    if (expTarget != null) {
                        val target = findExpandable(expTarget) as? Expandable
                        target?.registerExtPoint(classInst as KClass<out ExtPoint>)
                        val extPoint = classInst.primaryConstructor?.call(target) as ExtPoint
                        findExpandable(expTarget)?.installExtension(extPoint)
                    }
                }
            } catch (exc: kotlin.reflect.jvm.internal.KotlinReflectionInternalError) {}
        }
    }
    
    override fun createPrimaryScene(view: UIComponent): Scene {
        primeView = (view as MainView)
        return super.createPrimaryScene(view)
    }

    override val editor: Editor
        get() = primeView.editor

    override val installedToolGroups = arrayListOf<ToolGroup>()

    override val treeViewFactory = TreeViewFactory()
    override val nodeViewFactory = NodeViewFactory()

    override val treePull = arrayListOf(
        BinaryTreeFactory(),
        AVLTreeFactory(),
        RBTreeFactory()
    )
    override val availableInputFactories = arrayListOf(
        AnyInputFactory(),
        IntInputFactory(),
        StringInputFactory(),
        ArrayListInputFactory(),
        ImageInputFactory()
    )

    override fun addTool(tool: Tool) {
        primeView.addTool(tool)
    }

    override fun pushToStack(view: View) {
        primeView.stackPane.children.add(view.root)
    }

    override fun popFromStack() {
        primeView.stackPane.children.removeAt(primeView.stackPane.children.size-1)
    }

    override fun blurBack() {
        primeView.blurEffect.radius = 5.0
    }

    override fun focusBack() {
        primeView.blurEffect.radius = 0.0
    }
}
package example.andrew.sdk

import kotlin.reflect.KClass

annotation class DefaultExtension

annotation class ToolGroupTarget(val target: KClass<out ToolGroup>)

annotation class ExpTarget(val target: KClass<out Expandable>)
package example.andrew.sdk

import javafx.beans.property.BooleanProperty
import javafx.event.EventTarget

abstract class Tool(val app: OotpispApp?,
                    val name: String,
                    val tooltip: String = name,
                    val icoPath: String,
                    val disableProperty: BooleanProperty,
                    var toolGroup: ToolGroup = undefinedGroup): Expandable() {

    abstract fun action(source: EventTarget)
}
package example.andrew.sdk

import kotlin.reflect.KClass
import kotlin.reflect.full.isSubclassOf

interface ExtPoint {

    val parent: Expandable
}

abstract class Expandable {

    val extensions = arrayListOf<ExtPoint>()
    val availableExtensions = arrayListOf<KClass<out ExtPoint>>()

    inline fun <reified T: ExtPoint> registerExtPoint() {
        availableExtensions.add(T::class)
        onExtensionRegistered(T::class)
    }

    fun registerExtPoint(type: KClass<out ExtPoint>) {
        availableExtensions.add(type)
        onExtensionRegistered(type)
    }

    fun <T: ExtPoint> installExtension(ext: T) {
        val type = availableExtensions.find { ext::class.isSubclassOf(it) || ext::class == it }
        if (type != null) {
            extensions.add(ext)
            onExtensionInstalled(ext)
        }
    }

    fun <T: ExtPoint> installExtensions(vararg exts: T) {
        for (ext in exts) {
            installExtension(ext)
        }
    }

    open fun onExtensionInstalled(ext: ExtPoint) {}

    open fun onExtensionRegistered(type: KClass<out ExtPoint>) {}
}
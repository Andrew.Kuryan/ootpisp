package example.andrew.sdk

import example.andrew.sdk.trees.AbstractBinaryTreeFactory
import example.andrew.sdk.views.AbstractEditor
import example.andrew.sdk.views.AbstractNodeViewFactory
import example.andrew.sdk.views.AbstractTreeViewFactory
import example.andrew.sdk.views.InputFactory
import tornadofx.*
import kotlin.reflect.KClass
import kotlin.reflect.full.isSubclassOf

interface OotpispApp {

    val editor: AbstractEditor
    val installedToolGroups: ArrayList<out ToolGroup>

    val treeViewFactory: AbstractTreeViewFactory
    val nodeViewFactory: AbstractNodeViewFactory

    val availableInputFactories: ArrayList<InputFactory>
    val treePull: ArrayList<out AbstractBinaryTreeFactory>

    fun addTool(tool: Tool)

    fun pushToStack(view: View)
    fun popFromStack()

    fun blurBack()
    fun focusBack()
}

fun OotpispApp.findExpandable(type: KClass<out Expandable>) =
        installedToolGroups.find { it::class == type } ?:
        installedToolGroups.find { it.tools.find { it::class == type } != null }

inline fun <reified T: ToolGroup> OotpispApp.findToolGroup() = installedToolGroups.find { it is T }

fun OotpispApp.findToolGroup(type: KClass<out ToolGroup>) = installedToolGroups.find { it::class == type }

inline fun <reified T: Tool> ToolGroup.findTool() = this.tools.find { it is T }

fun ToolGroup.findTool(type: KClass<out Tool>) = this.tools.find { it::class == type }
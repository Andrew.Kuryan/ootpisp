package example.andrew.sdk.views

import example.andrew.sdk.trees.AbstractBinaryTree
import example.andrew.sdk.trees.AbstractBinaryTreeFactory
import javafx.scene.Node

interface AbstractTreeViewFactory {

    fun createTreeView(treeFactory: AbstractBinaryTreeFactory,
                       keyFactory: InputFactory,
                       valueFactory: InputFactory): AbstractTreeView
}

interface AbstractTreeView {

    val root: Node
    val keyFactory: InputFactory
    val valueFactory: InputFactory

    val treeFactory: AbstractBinaryTreeFactory
    var tree: AbstractBinaryTree<Comparable<Any>, Any>?

    fun repaint()
}
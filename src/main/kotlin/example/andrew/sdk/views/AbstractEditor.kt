package example.andrew.sdk.views

import example.andrew.sdk.trees.AbstractBinaryTreeFactory
import javafx.beans.property.BooleanProperty

data class CreateFormResult(val treeType: AbstractBinaryTreeFactory,
                            val keyType: InputFactory,
                            val valueType: InputFactory)

interface AbstractEditor {

    val activeTreeView: AbstractTreeView
    val treeTabProperty: BooleanProperty

    fun closeActiveTab()
    fun createTree(result: CreateFormResult)
    fun createNewTab(view: AbstractTreeView, treeName: String)
}
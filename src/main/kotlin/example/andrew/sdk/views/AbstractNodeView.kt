package example.andrew.sdk.views

import example.andrew.sdk.trees.AbstractBinaryNode
import javafx.scene.layout.Pane

interface AbstractNodeViewFactory {

    fun createNodeView(node: AbstractBinaryNode<Comparable<Any>, Any>): AbstractNodeView
}

interface AbstractNodeView {

    val root: Pane

    fun onClickAction()
}
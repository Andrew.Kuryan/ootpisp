package example.andrew.sdk.views

class InputException(val error: String) : Exception()

interface AbstractInput {

    fun getValue(): Any

    fun setValue(value: Any)
}
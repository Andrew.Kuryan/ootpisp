package example.andrew.sdk.views

import javafx.scene.Node
import kotlin.reflect.KClass

interface InputFactory {

    val type: KClass<*>

    fun createInput(editable: Boolean): Node
}
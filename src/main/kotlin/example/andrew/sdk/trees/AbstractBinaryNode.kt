package example.andrew.sdk.trees

interface AbstractBinaryNode<K : Comparable<K>, V: Any> {

    var key: K
    var value: V
    val height: Int

    operator fun compareTo(node: AbstractBinaryNode<K, V>): Int
    fun getLeft(): AbstractBinaryNode<K, V>?
    fun getRight(): AbstractBinaryNode<K, V>?
}
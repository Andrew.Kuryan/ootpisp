package example.andrew.sdk.trees

import kotlin.reflect.KClass

class ExistingKeyException(val key: Comparable<*>) : Exception()

class NoSuchKeyException(val key: Comparable<*>) : Exception()

interface AbstractBinaryTreeFactory {

    val treeType: KClass<*>
    val treeName: String

    fun createTree(key: Comparable<Any>, value: Any): AbstractBinaryTree<Comparable<Any>, Any>
    fun createTreeNode(key: Comparable<Any>, value: Any): AbstractBinaryNode<Comparable<Any>, Any>
}

interface AbstractBinaryTree<K : Comparable<K>, V: Any> {

    fun getRoot(): AbstractBinaryNode<K, V>?

    fun insert(key: K, value: V)
    fun remove(key: K)
    fun find(key: K): AbstractBinaryNode<K, V>
    fun clear()

    fun getHeight(): Int
    infix fun levelOf(node: AbstractBinaryNode<*, *>): Int
    fun RAB(): ArrayList<AbstractBinaryNode<*, *>>
}
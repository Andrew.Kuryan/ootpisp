package example.andrew.sdk

import kotlin.reflect.KClass
import kotlin.reflect.full.isSubclassOf

abstract class ToolGroup(app: OotpispApp?,
                         val name: String): Expandable() {

    val tools = arrayListOf<Tool>()
    val availableTools = arrayListOf<KClass<out Tool>>()

    inline fun <reified T: Tool> registerTool() {
        availableTools.add(T::class)
    }

    fun registerTool(type: KClass<out Tool>) {
        availableTools.add(type)
    }

    fun installTool(tool: Tool) {
        val type = availableTools.find { tool::class.isSubclassOf(it) || tool::class == it }
        if (type != null) {
            tools.add(tool)
            tool.toolGroup = this
            for (extPoint in availableExtensions) {
                tool.registerExtPoint(extPoint)
            }
            for (ext in extensions) {
                tool.installExtension(ext)
            }
            onToolInstalled(tool)
        }
    }

    fun installTools(vararg tools: Tool) {
        for (tool in tools) {
            installTool(tool)
        }
    }

    open fun onToolInstalled(tool: Tool) {}

    override fun onExtensionInstalled(ext: ExtPoint) {
        for (tool in tools) {
            tool.installExtensions(ext)
        }
    }

    override fun onExtensionRegistered(type: KClass<out ExtPoint>) {
        for (tool in tools) {
            tool.registerExtPoint(type)
        }
    }
}

val undefinedGroup = object: ToolGroup(null, "Undefined group") {}
package example.andrew.tools.app_group

import example.andrew.sdk.OotpispApp
import example.andrew.sdk.ToolGroup
import example.andrew.tools.app_group.new_tab.NewTabTool

class AppToolGroup(app: OotpispApp): ToolGroup(app, "App group") {

    init {
        registerTool<NewTabTool>()

        installTool(NewTabTool(app))
    }
}
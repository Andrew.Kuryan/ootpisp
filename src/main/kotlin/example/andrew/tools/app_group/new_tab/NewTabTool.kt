package example.andrew.tools.app_group.new_tab

import example.andrew.sdk.views.CreateFormResult
import example.andrew.sdk.OotpispApp
import example.andrew.sdk.Tool
import javafx.beans.property.SimpleBooleanProperty
import javafx.event.EventTarget

class NewTabTool(app: OotpispApp): Tool(app,
                       name = "Add new tree",
                       icoPath = "file:./src/main/resources/drawable/tab-new.png",
                       disableProperty = SimpleBooleanProperty(false)) {

    override fun action(source: EventTarget) {
        app!!.pushToStack(CreateDialogView(::onCreateTreeClick))
        app.blurBack()
    }

    private fun onCreateTreeClick(result: CreateFormResult) {
        app!!.editor.createTree(result)
        app.popFromStack()
        app.focusBack()
    }
}
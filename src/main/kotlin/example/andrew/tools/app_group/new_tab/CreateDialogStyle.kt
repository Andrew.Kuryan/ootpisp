package example.andrew.tools.app_group.new_tab

import example.andrew.ui.values.Colors
import example.andrew.ui.values.Colors.cdBackColor
import example.andrew.ui.values.Fonts
import example.andrew.ui.values.labelFont
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.layout.BackgroundPosition
import javafx.scene.layout.BackgroundRepeat
import javafx.scene.layout.BackgroundSize
import tornadofx.*
import java.net.URI

class CreateDialogStyle : Stylesheet() {

    companion object {
        val cdMainPane by cssclass()

        val mainBox by cssclass()
        val typesBox by cssclass()
        val itemGroup by cssclass()

        val labTitle by cssclass()
        val labParagraph by cssclass()
    }

    init {
        cdMainPane {
            prefWidth = 1200.px; prefHeight = 800.px
            backgroundColor += cdBackColor
            orientation = Orientation.VERTICAL
            alignment = Pos.TOP_CENTER
            button {
                prefWidth = 250.px; prefHeight = 50.px
                labelFont(Fonts.cdParagraphFont)
            }
        }
        labTitle and label {
            labelFont(Fonts.cdTitleFont)
        }
        labParagraph and label {
            labelFont(Fonts.cdParagraphFont)
        }
        radioButton {
            radio {
                focusColor = Colors.mainBorderColor
                faintFocusColor = Colors.mainBorderColor
                backgroundRadius += box(0.px)
                backgroundColor += Colors.mainBorderColor
                prefHeight = 16.px; prefWidth = 16.px
                backgroundColor += Colors.mainBorderColor
            }
            labelFont(Fonts.cdItemFont)
        }
        radioButton and selected {
            dot {
                backgroundColor += Colors.mainBorderColor
                backgroundRepeat += BackgroundRepeat.NO_REPEAT to BackgroundRepeat.NO_REPEAT
                backgroundPosition += BackgroundPosition.CENTER
                backgroundSize += BackgroundSize(16.0, 16.0,
                        false, false, false, false)
                backgroundRadius += box(0.px)
                backgroundImage += URI.create("file:./src/main/resources/drawable/object-select.png")
            }
        }
        mainBox {
            spacing = 20.px
            alignment = Pos.TOP_CENTER
        }
        typesBox {
            spacing = 30.px
        }
        itemGroup {
            spacing = 10.px
        }
    }
}
package example.andrew.tools.app_group.new_tab

import example.andrew.sdk.OotpispApp
import example.andrew.sdk.trees.AbstractBinaryTreeFactory
import example.andrew.sdk.views.CreateFormResult
import example.andrew.sdk.views.InputFactory
import javafx.scene.control.ToggleGroup
import javafx.scene.layout.VBox
import tornadofx.*
import kotlin.reflect.full.allSuperclasses

class CreateDialogView(callback: (result: CreateFormResult) -> Unit):
        View("Create Dialog") {

    private val keyPull: List<InputFactory>
    private val valuePull: List<InputFactory>

    init {
        importStylesheet(CreateDialogStyle::class)
        keyPull = (app as OotpispApp).availableInputFactories.filter { ifact ->
            ifact.type.allSuperclasses.find { it == Comparable::class } != null
        }
        valuePull = (app as OotpispApp).availableInputFactories.filter {
            it.type != Any::class
        }
    }

    private lateinit var tgTrees: ToggleGroup
    private lateinit var tgKeys: ToggleGroup
    private lateinit var tgValues: ToggleGroup

    override val root = flowpane {
        addClass(CreateDialogStyle.cdMainPane)
        anchorpane {
            vbox {
                label("Create new tree") {
                    addClass(CreateDialogStyle.labTitle)
                    VBox.setMargin(this, insets(0, 0, 80, 0))
                }
                addClass(CreateDialogStyle.mainBox)
                anchorpaneConstraints {
                    topAnchor = 100.0
                    bottomAnchor = 0.0
                    rightAnchor = 0.0
                    leftAnchor = 0.0
                }
                vbox {
                    addClass(CreateDialogStyle.itemGroup)
                    label("Select tree type:") { addClass(CreateDialogStyle.labParagraph) }
                    tgTrees = togglegroup {
                        var k = 0
                        for (construct in (app as OotpispApp).treePull) {
                            radiobutton(construct.treeName) {
                                userData = construct
                                if (k == 0) {
                                    isSelected = true
                                    k++
                                }
                            }
                        }
                    }
                }
                hbox {
                    addClass(CreateDialogStyle.typesBox)
                    vbox {
                        addClass(CreateDialogStyle.itemGroup)
                        label("Select key type:") { addClass(CreateDialogStyle.labParagraph) }
                        tgKeys = togglegroup {
                            var k = 0
                            for (input in keyPull) {
                                radiobutton(input.type.simpleName) {
                                    userData = input
                                    if (k == 0) {
                                        isSelected = true
                                        k++
                                    }
                                }
                            }
                        }
                    }
                    vbox {
                        addClass(CreateDialogStyle.itemGroup)
                        label("Select value type:") { addClass(CreateDialogStyle.labParagraph) }
                        tgValues = togglegroup {
                            var k = 0
                            for (input in valuePull) {
                                radiobutton(input.type.simpleName) {
                                    userData = input
                                    if (k == 0) {
                                        isSelected = true
                                        k++
                                    }
                                }
                            }
                        }
                    }
                }
                button("Create") {
                    VBox.setMargin(this, insets(30, 0, 0, 0))
                    setOnMouseClicked {
                        callback(CreateFormResult(
                                tgTrees.selectedToggle.userData as AbstractBinaryTreeFactory,
                                tgKeys.selectedToggle.userData as InputFactory,
                                tgValues.selectedToggle.userData as InputFactory
                        ))
                    }
                }
            }
        }
    }
}
package example.andrew.tools.tree_group

import example.andrew.sdk.OotpispApp
import example.andrew.sdk.ToolGroup
import example.andrew.tools.tree_group.add_node.AddNodeTool
import example.andrew.tools.tree_group.clear_tree.ClearTreeTool
import example.andrew.tools.tree_group.find_node.FindNodeTool
import example.andrew.tools.tree_group.remove_node.RemoveNodeTool

class TreeToolGroup(app: OotpispApp): ToolGroup(app, "Tree group") {

    init {
        registerTool<AddNodeTool>()
        registerTool<ClearTreeTool>()
        registerTool<FindNodeTool>()
        registerTool<RemoveNodeTool>()

        installTool(AddNodeTool(app))
        installTool(ClearTreeTool(app))
        installTool(FindNodeTool(app))
        installTool(RemoveNodeTool(app))
    }
}
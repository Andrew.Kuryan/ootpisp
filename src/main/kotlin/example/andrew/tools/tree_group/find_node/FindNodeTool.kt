package example.andrew.tools.tree_group.find_node

import example.andrew.sdk.OotpispApp
import example.andrew.sdk.Tool
import example.andrew.sdk.trees.NoSuchKeyException
import example.andrew.sdk.views.AbstractInput
import example.andrew.sdk.views.InputException
import example.andrew.tools.tree_group.FindNodeView
import javafx.application.Platform
import javafx.event.EventTarget

class FindNodeTool(app: OotpispApp): Tool(app = app,
            name = "Find node by key",
            icoPath = "file:./src/main/resources/drawable/edit-find.png",
            disableProperty = app.editor.treeTabProperty) {

    override fun action(source: EventTarget) {
        FindNodeView(action = "Find",
                keyFactory = app!!.editor.activeTreeView.keyFactory,
                callback = ::onFindConfirmClick).openModal(block = true)
    }

    private fun onFindConfirmClick(input: AbstractInput,
                                   success: () -> Unit,
                                   error: (err: String) -> Unit) {
        val key = try {
            input.getValue()
        } catch (exc: InputException) {
            error("Key ${exc.error}")
            null
        }
        if (key != null) {
            try {
                val node = app!!.editor.activeTreeView.tree!!.find(key as Comparable<Any>)
                success()
                Platform.runLater {
                    app.nodeViewFactory.createNodeView(node).onClickAction()
                }
            } catch (exc: NoSuchKeyException) {
                error("Key ${exc.key} does not exists")
            }
        }
    }
}
package example.andrew.tools.tree_group

import example.andrew.sdk.views.InputFactory
import example.andrew.sdk.views.AbstractInput
import example.andrew.ui.styles.NodeEditStyle
import javafx.scene.control.Label
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import tornadofx.*

class FindNodeView(action: String,
                   keyFactory: InputFactory,
                   callback: (input: AbstractInput,
                              success: ()->Unit,
                              error: (text: String)->Unit) -> Unit): View("$action dialog") {

    init {
        importStylesheet(ClearDialogStyle::class)
        importStylesheet(NodeEditStyle::class)
    }

    lateinit var labErr: Label

    override val root = vbox {
        addClass(ClearDialogStyle.cldlgMainPane)
        label("Input node's key:") {
            addClass(ClearDialogStyle.labMessage)
            VBox.setVgrow(this, Priority.ALWAYS)
        }
        val input = keyFactory.createInput(editable = true)
        add(input)
        labErr = label {
            addClass(NodeEditStyle.labError)
            isVisible = false
        }
        hbox {
            addClass(ClearDialogStyle.dialogButtonBox)
            button("Cancel") {
                setOnMouseClicked {
                    currentStage?.close()
                }
            }
            button("Find") {
                setOnMouseClicked {
                    callback(input as AbstractInput, {
                        currentStage?.close()
                    }, {
                        labErr.text = it
                        labErr.isVisible = true
                    })
                }
            }
        }
    }
}
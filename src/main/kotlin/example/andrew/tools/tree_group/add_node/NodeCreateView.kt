package example.andrew.tools.tree_group.add_node

import example.andrew.sdk.views.AbstractTreeView
import example.andrew.sdk.views.AbstractInput
import example.andrew.ui.styles.NodeEditStyle
import javafx.scene.control.Label
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import tornadofx.*

class NodeCreateView(tree: AbstractTreeView,
                     callback: (keyField: AbstractInput,
                                valueField: AbstractInput,
                                success: () -> Unit,
                                error: (err: String) -> Unit) -> Unit): View("New node") {

    init {
        importStylesheet(NodeEditStyle::class)
    }

    lateinit var labErr: Label

    override val root = vbox {
        addClass(NodeEditStyle.neMainPane)

        label("Key:")
        val keyInput = tree.keyFactory.createInput(editable = true)
        add(keyInput)

        label("Value:")
        val valueInput = tree.valueFactory.createInput(editable = true)
        valueInput.apply {
            VBox.setVgrow(this, Priority.ALWAYS)
        }
        add(valueInput)
        labErr = label {
            addClass(NodeEditStyle.labError)
            isVisible = false
        }
        anchorpane {
            addClass(NodeEditStyle.butContainer)
            VBox.setVgrow(this, Priority.ALWAYS)
            button("Create") {
                anchorpaneConstraints {
                    bottomAnchor = 0.0
                    leftAnchor = 0.0
                    rightAnchor = 0.0
                }
                setOnMouseClicked {
                    callback((keyInput as AbstractInput), (valueInput as AbstractInput),
                            /*success*/ {
                                labErr.text = ""
                                labErr.isVisible = false
                                currentStage?.close()
                            }, /*error*/ {
                                labErr.text = it
                                labErr.isVisible = true
                            }
                    )
                }
            }
        }
    }
}

package example.andrew.tools.tree_group.add_node

import example.andrew.sdk.OotpispApp
import example.andrew.sdk.Tool
import example.andrew.sdk.trees.ExistingKeyException
import example.andrew.sdk.views.AbstractInput
import example.andrew.sdk.views.InputException
import javafx.event.EventTarget

class AddNodeTool(app: OotpispApp): Tool(app = app,
                        name = "Add new node",
                        icoPath = "file:./src/main/resources/drawable/list-add.png",
                        disableProperty = app.editor.treeTabProperty) {

    override fun action(source: EventTarget) {
        NodeCreateView(app!!.editor.activeTreeView, callback = ::onCreateNodeClick)
                .apply {
                    openModal(block = true)
                }
    }

    private fun onCreateNodeClick(keyField: AbstractInput,
                                  valueField: AbstractInput,
                                  success: () -> Unit,
                                  error: (err: String) -> Unit) {
        @Suppress("UNCHECKED_CAST")
        val key = try {
            keyField.getValue() as Comparable<Any>
        } catch (exc: InputException) {
            error("Key ${exc.error}")
            null
        }
        val value = try {
            valueField.getValue()
        } catch (exc: InputException) {
            error("Value ${exc.error}")
            null
        }
        if (key != null && value != null) {
            val treeView = app!!.editor.activeTreeView
            if (treeView.tree == null) {
                treeView.tree = treeView.treeFactory.createTree(key, value)
                success()
                treeView.repaint()
            } else {
                try {
                    treeView.tree!!.insert(key, value)
                    success()
                    treeView.repaint()
                } catch (exc: ExistingKeyException) {
                    error("Key ${exc.key} already exists")
                }
            }
        }
    }
}
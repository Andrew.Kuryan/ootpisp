package example.andrew.tools.tree_group.remove_node

import example.andrew.sdk.OotpispApp
import example.andrew.sdk.Tool
import example.andrew.sdk.trees.NoSuchKeyException
import example.andrew.sdk.views.AbstractInput
import example.andrew.sdk.views.InputException
import example.andrew.tools.tree_group.FindNodeView
import javafx.event.EventTarget

class RemoveNodeTool(app: OotpispApp): Tool(app = app,
                name = "Remove node by key",
                icoPath = "file:./src/main/resources/drawable/list-remove.png",
                disableProperty = app.editor.treeTabProperty) {

    override fun action(source: EventTarget) {
        FindNodeView(action = "Remove",
                keyFactory = app!!.editor.activeTreeView.keyFactory,
                callback = ::onRemoveConfirmClick).openModal(block = true)
    }

    private fun onRemoveConfirmClick(input: AbstractInput,
                                     success: () -> Unit,
                                     error: (err: String) -> Unit) {
        val key = try {
            input.getValue()
        } catch (exc: InputException) {
            error("Key ${exc.error}")
            null
        }
        if (key != null) {
            try {
                app!!.editor.activeTreeView.tree?.remove(key as Comparable<Any>)
                success()
                app.editor.activeTreeView.repaint()
            } catch (exc: NoSuchKeyException) {
                error("Key ${exc.key} does not exists")
            }
        }
    }
}
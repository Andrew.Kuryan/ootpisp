package example.andrew.tools.tree_group

import example.andrew.ui.values.Colors
import example.andrew.ui.values.Fonts
import example.andrew.ui.values.labelFont
import javafx.geometry.Pos
import javafx.scene.text.TextAlignment
import tornadofx.*

class ClearDialogStyle : Stylesheet() {

    companion object {
        val cldlgMainPane by cssclass()
        val dialogButtonBox by cssclass()
        val labMessage by cssclass()
    }

    init {
        labMessage and label {
            wrapText = true
            textAlignment = TextAlignment.JUSTIFY
            labelFont(Fonts.cdParagraphFont)
        }
        cldlgMainPane {
            alignment = Pos.TOP_CENTER
            prefWidth = 350.px; prefHeight = 140.px
            padding = box(10.px)
            spacing = 25.px
            backgroundColor += Colors.controlColor
            button {
                prefWidth = 100.px
                prefHeight = 30.px
            }
        }
        dialogButtonBox {
            spacing = 10.px
            alignment = Pos.BOTTOM_RIGHT
        }
    }
}
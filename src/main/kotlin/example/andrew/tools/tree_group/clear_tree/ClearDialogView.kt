package example.andrew.tools.tree_group.clear_tree

import example.andrew.tools.tree_group.ClearDialogStyle
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import tornadofx.*

class ClearDialogView(callback: () -> Unit) : View("Clear dialog") {

    init {
        importStylesheet(ClearDialogStyle::class)
    }

    override val root = vbox {
        addClass(ClearDialogStyle.cldlgMainPane)
        label("Are you sure you want to clear the tree?") {
            addClass(ClearDialogStyle.labMessage)
            VBox.setVgrow(this, Priority.ALWAYS)
        }
        hbox {
            addClass(ClearDialogStyle.dialogButtonBox)
            button("Cancel") {
                setOnMouseClicked {
                    currentStage?.close()
                }
            }
            button("Clear") {
                setOnMouseClicked {
                    callback()
                    currentStage?.close()
                }
            }
        }
    }
}

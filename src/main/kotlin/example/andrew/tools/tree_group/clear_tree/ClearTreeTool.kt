package example.andrew.tools.tree_group.clear_tree

import example.andrew.sdk.OotpispApp
import example.andrew.sdk.Tool
import javafx.event.EventTarget
import javafx.scene.control.Button
import javafx.scene.control.Tab

class ClearTreeTool(app: OotpispApp): Tool(app = app,
                name = "Clear all tree",
                icoPath = "file:./src/main/resources/drawable/edit-delete.png",
                disableProperty = app.editor.treeTabProperty) {

    private var needToCloseTab: Boolean = false

    override fun action(source: EventTarget) {
        if (source is Tab) {
            needToCloseTab = true
        } else if (source is Button) {
            needToCloseTab = false
        }
        ClearDialogView(::onConfirmClearClick).apply {
            openModal(block = true)
        }
    }

    private fun onConfirmClearClick() {
        app!!.editor.activeTreeView.tree?.clear()
        app.editor.activeTreeView.repaint()
        if (needToCloseTab) {
            app.editor.closeActiveTab()
        }
    }
}
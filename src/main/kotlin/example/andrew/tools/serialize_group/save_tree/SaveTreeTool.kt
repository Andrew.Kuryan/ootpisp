package example.andrew.tools.serialize_group.save_tree

import example.andrew.sdk.OotpispApp
import example.andrew.sdk.Tool
import example.andrew.tools.serialize_group.ext_points.SerializeException
import example.andrew.tools.serialize_group.InfoView
import example.andrew.tools.serialize_group.ext_points.SaveMiddleware
import example.andrew.tools.serialize_group.ext_points.TreeSerializer
import example.andrew.tools.serialize_group.extensions.save_middleware.SaveMiddleView
import javafx.event.EventTarget
import javafx.stage.FileChooser
import tornadofx.*
import java.io.FileOutputStream
import java.io.OutputStream
import java.lang.Exception
import kotlin.reflect.KClass

class SaveTreeTool(app: OotpispApp): Tool(app = app,
                name = "Save tree",
                icoPath = "file:./src/main/resources/drawable/document-save-as.png",
                disableProperty = app.editor.treeTabProperty) {

    override fun action(source: EventTarget) {
        val saveFormats = extensions.filter { it is TreeSerializer }.map { it as TreeSerializer }
        val saveExtensions = saveFormats.map {
            FileChooser.ExtensionFilter(it.formatName, it.fileExtension)
        }.toTypedArray()

        var selectedExt = saveExtensions[0].extensions[0].trimStart('*', '.')
        val file = chooseFile("Choose file", saveExtensions, FileChooserMode.Save) {
            this.selectedExtensionFilterProperty().addListener { _, _, newValue ->
                selectedExt = newValue.extensions[0].trimStart('*', '.')
            }
        }
        if (file.isEmpty()) {
            InfoView("No file selected").openModal(block = true)
        } else {
            fun onSaveTypeChoose(middle: SaveMiddleware?) {
                var fileName = if (file[0].extension != selectedExt) {
                    "${file[0].name}.$selectedExt"
                } else { file[0].name }
                val serializer = saveFormats.find { it.fileExtension.trimStart('*', '.') == selectedExt }
                try {
                    var bytes = serializer!!.serialize(app!!.editor.activeTreeView.tree)
                    if (middle != null) {
                        val(newName, byteRes) = middle.saveHandler(fileName, bytes)
                        bytes = byteRes
                        fileName = newName
                    }
                    val os = FileOutputStream("${file[0].parent}/$fileName")
                    os.write(bytes)
                    os.flush()
                    os.close()
                    InfoView("Tree saved successfully").openModal(block = true)
                } catch (exc: Exception) {
                    exc.printStackTrace()
                    InfoView("An error occurred while saving tree").openModal(block = true)
                }
            }
            println(extensions)
            SaveMiddleView(extensions.filter {
                it is SaveMiddleware }.map { it as SaveMiddleware },
                ::onSaveTypeChoose
            ).openModal(block = true)
        }
    }
}
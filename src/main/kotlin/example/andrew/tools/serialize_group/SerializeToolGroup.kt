package example.andrew.tools.serialize_group

import example.andrew.sdk.OotpispApp
import example.andrew.sdk.ToolGroup
import example.andrew.tools.serialize_group.ext_points.SaveMiddleware
import example.andrew.tools.serialize_group.ext_points.TreeSerializer
import example.andrew.tools.serialize_group.extensions.tree_serializer.binary_serializer.BinaryTreeSerializer
import example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.custom.CustomTreeSerializer
import example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.json.JsonTreeSerializer
import example.andrew.tools.serialize_group.load_tree.LoadTreeTool
import example.andrew.tools.serialize_group.save_tree.SaveTreeTool

class SerializeToolGroup(app: OotpispApp): ToolGroup(app, "Serialize group") {

    init {
        registerTool<SaveTreeTool>()
        registerTool<LoadTreeTool>()

        installTool(LoadTreeTool(app))
        installTool(SaveTreeTool(app))

        registerExtPoint<TreeSerializer>()
        registerExtPoint<SaveMiddleware>()

        installExtension(BinaryTreeSerializer(this))
        installExtension(JsonTreeSerializer(this))
        installExtension(CustomTreeSerializer(this))
    }
}
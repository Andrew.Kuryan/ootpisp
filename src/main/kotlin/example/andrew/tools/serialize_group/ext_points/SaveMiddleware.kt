package example.andrew.tools.serialize_group.ext_points

import example.andrew.sdk.ExtPoint

interface SaveMiddleware: ExtPoint {

    val extName: String

    val fileExtension: String

    fun saveHandler(fileName: String, bytes: ByteArray): Pair<String, ByteArray>

    fun loadHandler(bytes: ByteArray): Pair<String, ByteArray>
}
package example.andrew.tools.serialize_group.ext_points

import example.andrew.sdk.trees.AbstractBinaryTree
import example.andrew.sdk.ExtPoint
import java.io.InputStream
import java.io.OutputStream

class SerializeException: Exception()

class ParseException: Exception()

interface TreeSerializer: ExtPoint {

    val fileExtension: String

    val formatName: String

    fun <K: Comparable<K>, V: Any> serialize(tree: AbstractBinaryTree<K, V>?): ByteArray

    fun parse(bytes: ByteArray): AbstractBinaryTree<Comparable<Any>, Any>
}

package example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.custom

import example.andrew.tools.serialize_group.ext_points.ParseException
import example.andrew.tools.serialize_group.ext_points.SerializeException
import example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.TextAdapterFactory
import kotlin.reflect.KClass

fun Any.toTextWithType(availableFactories: ArrayList<TextAdapterFactory<Any>>): String {
    val factory = availableFactories.find {this::class == it.type}
    factory ?: throw SerializeException()
    return factory.createAdapter().serialize(this)
}

fun String.fromTextWithType(availableFactories: ArrayList<TextAdapterFactory<Any>>, clazz: KClass<*>): Any {
    val factory = availableFactories.find {clazz == it.type}
    if (factory == null) { println(this); throw ParseException()
    }
    return factory.createAdapter().deserialize(this)
}
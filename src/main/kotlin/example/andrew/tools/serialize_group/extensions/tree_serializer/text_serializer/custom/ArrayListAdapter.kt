package example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.custom

import example.andrew.sdk.DefaultExtension
import example.andrew.sdk.Expandable
import example.andrew.tools.serialize_group.extensions.tree_serializer.lexer.Lexer
import example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.TextAdapter
import example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.TextAdapterFactory

@DefaultExtension
class ArrayListAdapterFactory(override val parent: Expandable): TextAdapterFactory<ArrayList<*>> {

    override val type = ArrayList::class

    override fun createAdapter() = ArrayListAdapter(parent)
}

@DefaultExtension
class ArrayListAdapter(override val parent: Expandable): TextAdapter<ArrayList<*>> {

    override fun <T> serialize(obj: T): String {
        return (obj as ArrayList<*>).joinToString(
                prefix = "[",
                postfix = "]",
                separator = ","
        ) { "\\$it\\" }
    }

    override fun deserialize(str: String): ArrayList<*> {
        val tokens = Lexer(ArrayDKAFactory()).analyze(str)
        return ArrayList(tokens.map { it.value })
    }
}
package example.andrew.tools.serialize_group.extensions.save_middleware

import example.andrew.tools.serialize_group.ext_points.SaveMiddleware
import example.andrew.ui.styles.NodeEditStyle
import javafx.application.Platform
import javafx.geometry.Orientation
import javafx.scene.control.ToggleGroup
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import tornadofx.*

class SaveMiddleView(availableMiddles: List<SaveMiddleware>,
                     callback: (selected: SaveMiddleware?) -> Unit):
        View("Save plugins") {

    init {
        importStylesheet(NodeEditStyle::class)
    }

    lateinit var toggleGroup: ToggleGroup

    override val root = vbox {
        addClass(NodeEditStyle.neMainPane)
        label("Select save plugin") {
            addClass(NodeEditStyle.labHeader)
        }
        separator {
            orientation = Orientation.HORIZONTAL
        }
        toggleGroup = togglegroup {
            radiobutton("Do not use") {
                userData = null
                isSelected = true
            }
            for (middle in availableMiddles) {
                radiobutton(middle.extName) {
                    userData = middle
                }
            }
        }

        anchorpane {
            VBox.setVgrow(this, Priority.ALWAYS)
            hbox {
                addClass(NodeEditStyle.butDialogContainer)
                anchorpaneConstraints {
                    bottomAnchor = 0.0
                    leftAnchor = 0.0
                    rightAnchor = 0.0
                }
                button("Cancel") {
                    setOnMouseClicked {
                        Platform.runLater {
                            currentStage?.close()
                        }
                        callback(null)
                    }
                }
                button("Choose") {
                    setOnMouseClicked {
                        Platform.runLater {
                            currentStage?.close()
                        }
                        callback(toggleGroup.selectedToggle.userData as? SaveMiddleware)
                    }
                }
            }
        }
    }
}

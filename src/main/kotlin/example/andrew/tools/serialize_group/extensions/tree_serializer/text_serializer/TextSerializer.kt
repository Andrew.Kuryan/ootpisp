package example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer

import example.andrew.sdk.DefaultExtension
import example.andrew.sdk.Expandable
import example.andrew.tools.serialize_group.ext_points.TreeSerializer

@DefaultExtension
abstract class TextSerializer(override val parent: Expandable): TreeSerializer, Expandable() {

    init  {
        registerExtPoint<TextSerializer>()
        registerExtPoint<TextAdapter<*>>()
        registerExtPoint<TextAdapterFactory<*>>()

        installExtension(RBColorAdapterFactory(this))
        installExtension(ImageAdapterFactory(this))
        installExtension(IntAdapterFactory(this))
        installExtension(StringAdapterFactory(this))
    }
}
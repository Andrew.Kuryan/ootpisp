package example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.custom

import example.andrew.tools.serialize_group.extensions.tree_serializer.lexer.DKA
import example.andrew.tools.serialize_group.extensions.tree_serializer.lexer.DKAFactory
import example.andrew.tools.serialize_group.extensions.tree_serializer.lexer.Token
import java.lang.StringBuilder

enum class ArrayTokenType {
    VALUE
}

class ArrayDKAFactory: DKAFactory {

    override fun createDKA(tokens: ArrayList<Token>): ArrayDKA {
        val dka = ArrayDKA(tokens)
        dka.generateJumpTable()
        return dka
    }
}

class ArrayDKA(tokens: ArrayList<Token>): DKA() {

    private val buffer = StringBuilder("")

    override val states = mapOf(
        "A" to emptyState,
        "B" to emptyState,
        "C" to { prev: String, sym: Char -> if (prev != "B") buffer.append(sym) },
        "D" to { _, _ -> tokens.add(Token(ArrayTokenType.VALUE, buffer.toString())); buffer.clear() },
        "E" to emptyState
    )

    override val startState = "A"

    override val endState = "E"

    override val jumps = mapOf(
        "[" to {sym: Char -> sym == '['},
        "]" to {sym: Char -> sym == ']'},
        "\\" to {sym: Char -> sym == '\\'},
        "," to {sym: Char -> sym == ','},
        "*" to {sym: Char -> sym != '\\'}
    )

    override val jumpList = arrayListOf(
        "A" - "[" - "B",
        "B" - "\\" - "C",
        "C" - "*" - "C",
        "C" - "\\" - "D",
        "D" - "," - "B",
        "D" - "]" - "E"
    )
}
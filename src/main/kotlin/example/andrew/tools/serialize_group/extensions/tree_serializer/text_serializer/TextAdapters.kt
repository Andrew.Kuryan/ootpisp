package example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer

import example.andrew.logic.nodes.Color
import example.andrew.sdk.DefaultExtension
import example.andrew.sdk.Expandable
import example.andrew.tools.serialize_group.ext_points.ParseException
import javafx.embed.swing.SwingFXUtils
import javafx.scene.image.Image
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.util.*
import javax.imageio.ImageIO

@DefaultExtension
class IntAdapter(override val parent: Expandable): TextAdapter<Int> {

    override fun <T> serialize(obj: T): String {
        return obj.toString()
    }

    override fun deserialize(str: String): Int {
        return str.toInt()
    }
}

@DefaultExtension
class StringAdapter(override val parent: Expandable): TextAdapter<String> {

    override fun <T> serialize(obj: T): String {
        return obj as String
    }

    override fun deserialize(str: String): String {
        return str
    }
}

@DefaultExtension
open class RBColorAdapter(override val parent: Expandable): TextAdapter<Color> {

    override fun <T> serialize(obj: T): String {
        return obj.toString()
    }

    override fun deserialize(str: String): Color {
        return when(str){
            "Red" -> Color.RED
            "Black" -> Color.BLACK
            else -> throw ParseException()
        }
    }
}

@DefaultExtension
open class ImageAdapter(override val parent: Expandable): TextAdapter<Image> {

    override fun<T> serialize(obj: T): String {
        val bos = ByteArrayOutputStream()
        ImageIO.write(SwingFXUtils.fromFXImage(obj as Image, null), "png", bos)
        val imageBytes = bos.toByteArray()
        val imageString = Base64.getEncoder().encodeToString(imageBytes)
        bos.close()
        return imageString
    }

    override fun deserialize(str: String): Image {
        val imageByteArray = Base64.getDecoder().decode(str)
        return Image(ByteArrayInputStream(imageByteArray))
    }
}
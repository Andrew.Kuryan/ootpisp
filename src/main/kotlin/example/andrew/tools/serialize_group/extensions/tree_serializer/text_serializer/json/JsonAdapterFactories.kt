package example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.json

import example.andrew.logic.nodes.Color
import example.andrew.sdk.DefaultExtension
import example.andrew.sdk.Expandable
import javafx.scene.image.Image

@DefaultExtension
class RBColorJsonAdapterFactory(override val parent: Expandable): JsonAdapterFactory<Color> {

    override val type = Color::class

    override fun createAdapter() = RBColorJsonAdapter(parent)
}

@DefaultExtension
class ImageJsonAdapterFactory(override val parent: Expandable): JsonAdapterFactory<Image> {

    override val type = Image::class

    override fun createAdapter() = ImageJsonAdapter(parent)
}
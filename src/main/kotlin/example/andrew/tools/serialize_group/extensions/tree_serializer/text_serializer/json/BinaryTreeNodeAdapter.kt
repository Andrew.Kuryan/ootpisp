package example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.json

import com.github.salomonbrys.kotson.*
import com.google.gson.*
import example.andrew.annotations.IgnoreTargets
import example.andrew.annotations.IgnoredField
import example.andrew.logic.nodes.BinaryTreeNode
import example.andrew.tools.serialize_group.ext_points.ParseException
import example.andrew.logic.trees.LEFT
import example.andrew.logic.trees.RIGHT
import example.andrew.sdk.DefaultExtension
import example.andrew.sdk.Expandable
import java.lang.reflect.Type
import kotlin.reflect.KClass
import kotlin.reflect.KVisibility
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.isSubclassOf
import kotlin.reflect.full.memberProperties
import kotlin.reflect.full.primaryConstructor
import kotlin.reflect.jvm.isAccessible
import kotlin.reflect.jvm.javaField
import kotlin.reflect.jvm.jvmErasure

@DefaultExtension
open class BinaryTreeNodeAdapter(override val parent: Expandable,
                                 private val nodeArray: JsonArray,
                                 private val nodeType: KClass<*>,
                                 private val nodeGenerics: Map<String, String>):
        JsonAdapter<BinaryTreeNode<Comparable<Any>, Any>>{

    private val adapterFactories = arrayListOf<JsonAdapterFactory<*>>()

    init {
        adapterFactories.addAll(parent.extensions
                .filter { it::class.isSubclassOf(JsonAdapterFactory::class) }
                .map { it as JsonAdapterFactory<*> })
    }

    @Suppress("UNCHECKED_CAST")
    open fun createInstance(json: JsonElement): BinaryTreeNode<Comparable<Any>, Any> {
        val primeParams = arrayListOf<Any>()
        for (k in json.asJsonObject.keys()) {
            if (k.startsWith('*')) {
                if (k.trimStart('*') in nodeGenerics.keys) {
                    val type = Class.forName(nodeGenerics.getValue(k.trimStart('*'))).kotlin
                    primeParams.add(json[k].fromJsonWithType(adapterFactories, type))
                } else {
                    val type = nodeType.primaryConstructor!!.parameters.find {
                        it.name == k.trimStart('*')
                    }?.type?.jvmErasure ?: Any::class
                    primeParams.add(json[k].fromJsonWithType(adapterFactories, type))
                }
            }
        }
        val node = nodeType.java.constructors[0].newInstance(*primeParams.toArray())
                as BinaryTreeNode<Comparable<Any>, Any>
        for (k in json.asJsonObject.keys()) {
            if (!k.startsWith('*') && k != "left" && k != "right") {
                val field = node::class.memberProperties.find{ it.name == k }
                if (field != null && field.isAccessible) {
                    field.javaField?.set(node, json[k].fromJsonWithType(adapterFactories, field.returnType.jvmErasure))
                } else {
                    throw ParseException()
                }
            }
        }
        return node
    }

    open fun serializeInstance(node: BinaryTreeNode<Comparable<Any>, Any>): JsonObject {
        val json = jsonObject()
        val primeParams = node::class.primaryConstructor!!.parameters
        //запись параметров первичного конструктора
        for (param in primeParams) {
            val prop = node::class.memberProperties.find { it.name == param.name }
            val propVal = prop!!.getter.call(node)
            json.addProperty("*${prop.name}", propVal?.toJsonWithType(adapterFactories))
        }
        //запись остальных параметров
        for (p in node::class.memberProperties) {
            if (p.visibility != KVisibility.PRIVATE) {
                if (primeParams.find { it.name == p.name } == null) {
                    val ignoreAnnot = p.findAnnotation<IgnoredField>()
                    if (ignoreAnnot == null ||
                            (ignoreAnnot.target != IgnoreTargets.SERIALIZE &&
                                    ignoreAnnot.target != IgnoreTargets.ALL)) {
                        val fieldValue = p.getter.call(node)
                        json.addProperty(p.name, fieldValue?.toJsonWithType(adapterFactories))
                    }
                }
            }
        }
        return json
    }

    override fun serialize(src: BinaryTreeNode<Comparable<Any>, Any>, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        val nodes = jsonArray()
        src.serializeRec(nodes)
        return nodes
    }

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): BinaryTreeNode<Comparable<Any>, Any> {
        val node = createInstance(json)
        val leftJson = json["left"]
        if (leftJson.asString == "NULL") {
            node.setChild(null, LEFT)
        } else {
            val keyType = Class.forName(nodeGenerics.getValue("key")).kotlin
            val leftNode = nodeArray.find { it["*key"].fromJsonWithType(adapterFactories, keyType) ==
                    leftJson.fromJsonWithType(adapterFactories, keyType) }
            node.setChild(deserialize(leftNode!!, typeOfT, context), LEFT)
        }
        val rightJson = json["right"]
        if (rightJson.asString == "NULL") {
            node.setChild(null, RIGHT)
        } else {
            val keyType = Class.forName(nodeGenerics.getValue("key")).kotlin
            val rightNode = nodeArray.find { it["*key"].fromJsonWithType(adapterFactories, keyType) ==
                    rightJson.fromJsonWithType(adapterFactories, keyType) }
            node.setChild(deserialize(rightNode!!, typeOfT, context), RIGHT)
        }
        return node
    }

    private fun BinaryTreeNode<Comparable<Any>, Any>.serializeRec(nodes: JsonArray): JsonArray {
        val json = serializeInstance(this)
        val left = this.getLeft()
        if (left == null) {
            json.addProperty("left", "NULL")
        } else {
            json.addProperty("left", left.key.toJsonWithType(adapterFactories))
        }
        val right = this.getRight()
        if (right == null) {
            json.addProperty("right", "NULL")
        } else {
            json.addProperty("right", right.key.toJsonWithType(adapterFactories))
        }
        nodes.add(json)
        left?.serializeRec(nodes)
        right?.serializeRec(nodes)
        return nodes
    }
}
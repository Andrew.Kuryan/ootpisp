package example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer

import example.andrew.sdk.ExtPoint
import kotlin.reflect.KClass

interface TextAdapterFactory<out T: Any>: ExtPoint {

    val type: KClass<out Any>

    fun createAdapter(): TextAdapter<out T>
}

interface TextAdapter<T: Any>: ExtPoint {

    fun <T> serialize(obj: T): String

    fun deserialize(str: String): T
}
package example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.json

import com.github.salomonbrys.kotson.*
import com.google.gson.*
import example.andrew.annotations.IgnoreTargets
import example.andrew.annotations.IgnoredField
import example.andrew.logic.nodes.BinaryTreeNode
import example.andrew.tools.serialize_group.ext_points.ParseException
import example.andrew.tools.serialize_group.ext_points.SerializeException
import example.andrew.logic.trees.BinaryTree
import example.andrew.sdk.DefaultExtension
import example.andrew.sdk.Expandable
import java.lang.reflect.Type
import kotlin.reflect.KVisibility
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.memberProperties
import kotlin.reflect.full.primaryConstructor
import kotlin.reflect.jvm.jvmName

@DefaultExtension
open class BinaryTreeAdapter(override val parent: Expandable): JsonAdapter<BinaryTree<*, *>> {

    override fun serialize(tree: BinaryTree<*, *>, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        val json = jsonObject()
        json.addProperty("type", tree::class.jvmName)
        val root = tree.getRoot()
        root ?: throw SerializeException()

        json.addProperty("nodeType", root::class.jvmName)
        //соответствие названий обобщенных полей к фактическим типам
        val typeMap = mutableMapOf<String, String>()
        val typeParamNames = root::class.typeParameters.map { it.name }
        for (p in root::class.memberProperties) {
            if (p.visibility != KVisibility.PRIVATE) {
                val ignoreAnnot = p.findAnnotation<IgnoredField>()
                if (ignoreAnnot == null ||
                        (ignoreAnnot.target != IgnoreTargets.SERIALIZE &&
                                ignoreAnnot.target != IgnoreTargets.ALL)) {
                    val fieldVal = p.getter.call(root)
                    if (fieldVal != null && p.returnType.toString() in typeParamNames) {
                        typeMap += p.name to fieldVal::class.jvmName
                    }
                }
            }
        }
        json.addProperty("nodeGenerics", Gson().toJsonTree(typeMap))
        val gson = GsonBuilder()
                .registerTypeAdapter(BinaryTreeNode::class.java,
                        BinaryTreeNodeAdapter(parent, jsonArray(), BinaryTreeNode::class, mapOf()))
                .create()
        json.addProperty("nodes", gson.toJsonTree(root, BinaryTreeNode::class.java))
        return json
    }

    @Suppress("UNCHECKED_CAST")
    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): BinaryTree<*, *> {
        val jsonNodes = json["nodes"].asJsonArray
        if (jsonNodes.size() == 0) {
            throw ParseException()
        }
        val root = jsonNodes[0]
        val nodeType = Class.forName(json["nodeType"].asString)
        val gson = GsonBuilder()
                .registerTypeAdapter(BinaryTreeNode::class.java,
                        BinaryTreeNodeAdapter(parent,
                                jsonNodes,
                                nodeType.kotlin,
                                Gson().fromJson(json["nodeGenerics"])))
                .create()
        val tree = Class.forName(json["type"].asString).kotlin.primaryConstructor!!.call()
                as BinaryTree<Comparable<Any>, Any>
        tree.root = gson.fromJson(root, BinaryTreeNode::class.java) as BinaryTreeNode<Comparable<Any>, Any>
        return tree
    }
}

package example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.json

import com.google.gson.*
import example.andrew.sdk.trees.AbstractBinaryTree
import example.andrew.sdk.DefaultExtension
import example.andrew.sdk.Expandable
import example.andrew.tools.serialize_group.ext_points.ParseException
import example.andrew.tools.serialize_group.ext_points.SerializeException
import example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.TextSerializer
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.nio.file.Files
import java.nio.file.Paths

@DefaultExtension
class JsonTreeSerializer(override val parent: Expandable): TextSerializer(parent) {

    init {
        registerExtPoint<JsonAdapter<*>>()
        registerExtPoint<JsonAdapterFactory<*>>()

        installExtension(RBColorJsonAdapterFactory(this))
        installExtension(ImageJsonAdapterFactory(this))
    }

    override val fileExtension = "*.json"

    override val formatName = "JSON"

    @Suppress("UNCHECKED_CAST")
    override fun parse(bytes: ByteArray): AbstractBinaryTree<Comparable<Any>, Any> {
        return try {
            val str = String(bytes)
            val gson = GsonBuilder()
                    .registerTypeAdapter(AbstractBinaryTree::class.java, BinaryTreeAdapter(this))
                    .create()
            gson.fromJson(str, AbstractBinaryTree::class.java) as AbstractBinaryTree<Comparable<Any>, Any>
        } catch (exc: Exception) {
            throw ParseException()
        }
    }

    override fun <K : Comparable<K>, V: Any> serialize(tree: AbstractBinaryTree<K, V>?): ByteArray {
        try {
            if (tree == null) {
                throw SerializeException()
            }
            val gson = GsonBuilder()
                    .registerTypeAdapter(AbstractBinaryTree::class.java, BinaryTreeAdapter(this))
                    .create()
            val bos = ByteArrayOutputStream()
            bos.write(gson.toJson(tree, AbstractBinaryTree::class.java).toByteArray())
            return bos.toByteArray()
        } catch (exc: Exception) {
            throw SerializeException()
        }
    }
}
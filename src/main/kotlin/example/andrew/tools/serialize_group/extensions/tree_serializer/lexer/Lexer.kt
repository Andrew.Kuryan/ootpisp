package example.andrew.tools.serialize_group.extensions.tree_serializer.lexer

import example.andrew.tools.serialize_group.ext_points.ParseException

class Lexer(private val DKAFactory: DKAFactory) {

    fun analyze(str: String): List<Token> {
        val tokens = arrayListOf<Token>()
        val DKA = DKAFactory.createDKA(tokens)
        var currentState = DKA.startState

        for (sym in str) {
            var flagTrans = false
            for ((i, state) in DKA.jumpTable.getValue(currentState).withIndex()) {
                if (state != null && DKA.jumpValues[i](sym)) {
                    DKA.states.getValue(state)(currentState, sym)
                    currentState = state
                    flagTrans = true
                    break
                }
            }
            if (!flagTrans) {
                throw ParseException()
            }
        }
        if (currentState != DKA.endState) {
            throw ParseException()
        }
        return tokens
    }
}
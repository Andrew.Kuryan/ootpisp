package example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.json

import com.github.salomonbrys.kotson.toJson
import com.google.gson.*
import example.andrew.logic.nodes.Color
import example.andrew.sdk.DefaultExtension
import example.andrew.sdk.Expandable
import example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.ImageAdapter
import example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.RBColorAdapter
import javafx.scene.image.Image
import java.lang.reflect.Type

@DefaultExtension
class RBColorJsonAdapter(override val parent: Expandable): RBColorAdapter(parent), JsonAdapter<Color> {

    override fun serialize(src: Color, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        return super.serialize(src).toJson()
    }

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): Color {
        return super.deserialize(json.asString)
    }
}

@DefaultExtension
class ImageJsonAdapter(override val parent: Expandable): ImageAdapter(parent), JsonAdapter<Image> {

    override fun serialize(src: Image, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        return super.serialize(src).toJson()
    }

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): Image {
        return super.deserialize(json.asString)
    }
}
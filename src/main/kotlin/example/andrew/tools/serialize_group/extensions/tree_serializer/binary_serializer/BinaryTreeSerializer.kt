package example.andrew.tools.serialize_group.extensions.tree_serializer.binary_serializer

import example.andrew.sdk.trees.AbstractBinaryTree
import example.andrew.sdk.DefaultExtension
import example.andrew.sdk.Expandable
import example.andrew.tools.serialize_group.ext_points.ParseException
import example.andrew.tools.serialize_group.ext_points.SerializeException
import example.andrew.tools.serialize_group.ext_points.TreeSerializer
import java.io.*

@DefaultExtension
class BinaryTreeSerializer(override val parent: Expandable): TreeSerializer {

    override val fileExtension = "*.bin"

    override val formatName = "Binary"

    @Suppress("UNCHECKED_CAST")
    override fun parse(bytes: ByteArray): AbstractBinaryTree<Comparable<Any>, Any> {
        return try {
            val ois = ObjectInputStream(ByteArrayInputStream(bytes))
            ois.readObject() as AbstractBinaryTree<Comparable<Any>, Any>
        } catch (exc: Exception) {
            exc.printStackTrace()
            throw ParseException()
        }
    }

    override fun <K : Comparable<K>, V: Any> serialize(tree: AbstractBinaryTree<K, V>?): ByteArray {
        try {
            val bos = ByteArrayOutputStream()
            val oos = ObjectOutputStream(bos)
            oos.writeObject(tree)
            return bos.toByteArray()
        } catch (exc: Exception) {
            exc.printStackTrace()
            throw SerializeException()
        }
    }
}
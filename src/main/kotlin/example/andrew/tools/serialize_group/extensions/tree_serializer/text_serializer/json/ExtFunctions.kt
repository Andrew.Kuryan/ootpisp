package example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.json

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import example.andrew.tools.serialize_group.ext_points.ParseException
import example.andrew.tools.serialize_group.ext_points.SerializeException
import kotlin.reflect.KClass

fun Any.toJsonWithType(availableFactories: ArrayList<JsonAdapterFactory<Any>>): JsonElement {
    val factory = availableFactories.find {this::class == it.type}
    return if (factory != null) {
        val gson = GsonBuilder()
                .registerTypeAdapter(factory.type.java, factory.createAdapter())
                .create()
        gson.toJsonTree(this, factory.type.java)
    } else {
        val default = Gson().toJsonTree(this) ?: throw SerializeException()
        default
    }
}
fun JsonElement.fromJsonWithType(availableFactories: ArrayList<JsonAdapterFactory<Any>>, clazz: KClass<*>): Any {
    val factory = availableFactories.find { clazz == it.type }
    return if (factory != null) {
        val gson = GsonBuilder()
                .registerTypeAdapter(factory.type.java, factory.createAdapter())
                .create()
        gson.fromJson(this, factory.type.java)
    } else {
        val default = Gson().fromJson(this, clazz.java) ?: throw ParseException()
        default
    }
}
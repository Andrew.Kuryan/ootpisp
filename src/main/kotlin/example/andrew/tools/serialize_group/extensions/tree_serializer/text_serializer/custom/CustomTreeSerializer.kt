package example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.custom

import example.andrew.annotations.IgnoreTargets
import example.andrew.annotations.IgnoredField
import example.andrew.tools.serialize_group.extensions.tree_serializer.lexer.Lexer
import example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.custom.analyzer.NodeRecord
import example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.custom.analyzer.Parser
import example.andrew.logic.trees.BinaryTree
import example.andrew.logic.nodes.BinaryTreeNode
import example.andrew.logic.trees.LEFT
import example.andrew.logic.trees.RIGHT
import example.andrew.sdk.trees.AbstractBinaryNode
import example.andrew.sdk.trees.AbstractBinaryTree
import example.andrew.sdk.DefaultExtension
import example.andrew.sdk.Expandable
import example.andrew.tools.serialize_group.ext_points.ParseException
import example.andrew.tools.serialize_group.ext_points.SerializeException
import example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.TextAdapterFactory
import example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.custom.analyzer.CustomDKAFactory
import example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.TextSerializer
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.reflect.KVisibility
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.isSubclassOf
import kotlin.reflect.full.memberProperties
import kotlin.reflect.full.primaryConstructor
import kotlin.reflect.jvm.isAccessible
import kotlin.reflect.jvm.javaField
import kotlin.reflect.jvm.jvmErasure
import kotlin.reflect.jvm.jvmName

@DefaultExtension
class CustomTreeSerializer(override val parent: Expandable): TextSerializer(parent) {

    private val adapterFactories = arrayListOf<TextAdapterFactory<*>>()

    init {
        installExtension(ArrayListAdapterFactory(this))
        adapterFactories.addAll(extensions
                .filter { it::class.isSubclassOf(TextAdapterFactory::class) }
                .map { it as TextAdapterFactory<*> })
    }

    override val fileExtension = "*.btree"

    override val formatName = "BinaryTree"

    @Suppress("UNCHECKED_CAST")
    override fun parse(bytes: ByteArray): AbstractBinaryTree<Comparable<Any>, Any> {
        return try {
            val str = String(bytes)
            val (header, body) = str.split("\n\n")
            val mapInfo = parseHeader(header)
            val tokens = Lexer(CustomDKAFactory()).analyze(body)
            val records = Parser().parse(tokens)
            if (records.size == 0) {
                throw ParseException()
            }
            val tree = Class.forName(mapInfo.getValue("treeType")).kotlin.primaryConstructor!!.call()
                                as BinaryTree<Comparable<Any>, Any>
            tree.root = records[0].deserialize(mapInfo, records)
            tree
        } catch (exc: Exception) {
            exc.printStackTrace()
            throw ParseException()
        }
    }

    private fun parseHeader(str: String): Map<String, String> {
        val res = mutableMapOf<String, String>()
        val records = str.split("\n")
        for (rec in records) {
            val (key, value) = rec.split("=")
            res += key to value
        }
        return res
    }

    private fun NodeRecord.deserialize(header: Map<String, String>, nodeArray: ArrayList<NodeRecord>):
            BinaryTreeNode<Comparable<Any>, Any> {
        val node = createInstance(header)
        val keyType = Class.forName(header.getValue("key")).kotlin
        if (left == "null") {
            node.setChild(null, LEFT)
        } else {
            val leftNode = nodeArray.find {
                it.primeProps.getValue("key").fromTextWithType(adapterFactories, keyType) ==
                        left.fromTextWithType(adapterFactories, keyType)
            }
            node.setChild(leftNode!!.deserialize(header, nodeArray), LEFT)
        }
        if (right == "null") {
            node.setChild(null, RIGHT)
        } else {
            val rightNode = nodeArray.find {
                it.primeProps.getValue("key").fromTextWithType(adapterFactories, keyType) ==
                        right.fromTextWithType(adapterFactories, keyType)
            }
            node.setChild(rightNode!!.deserialize(header, nodeArray), RIGHT)
        }
        return node
    }

    @Suppress("UNCHECKED_CAST")
    private fun NodeRecord.createInstance(header: Map<String, String>): BinaryTreeNode<Comparable<Any>, Any> {
        val primeParams = arrayListOf<Any>()
        val nodeType = Class.forName(header.getValue("nodeType")).kotlin
        for (k in primeProps) {
            if (k.key in header.keys) {
                val type = Class.forName(header.getValue(k.key)).kotlin
                primeParams.add(k.value.fromTextWithType(adapterFactories, type))
            } else {
                val type = nodeType.primaryConstructor!!.parameters.find {
                    it.name == k.key
                }?.type?.jvmErasure ?: Any::class
                primeParams.add(k.value.fromTextWithType(adapterFactories, type))
            }
        }
        val node = nodeType.java.constructors[0].newInstance(*primeParams.toArray())
                as BinaryTreeNode<Comparable<Any>, Any>
        for (k in secProps) {
            val field = node::class.memberProperties.find{ it.name == k.key }
            if (field != null && field.isAccessible) {
                field.javaField?.set(node, k.value.fromTextWithType(adapterFactories, field.returnType.jvmErasure))
            } else {
                throw ParseException()
            }
        }
        return node
    }

    override fun <K : Comparable<K>, V: Any> serialize(tree: AbstractBinaryTree<K, V>?): ByteArray {
        try {
            if (tree == null) {
                throw SerializeException()
            }
            val root = tree.getRoot()
            root ?: throw SerializeException()

            var res = tree.createFileHeader() + "\n\n"
            val nodes = arrayListOf<String>()
            res += root.serialize(nodes).joinToString("\n") + "\n"
            val bos = ByteArrayOutputStream()
            bos.write(res.toByteArray())
            return bos.toByteArray()
        } catch (exc: Exception) {
            exc.printStackTrace()
            throw SerializeException()
        }
    }

    private fun <K : Comparable<K>, V: Any> AbstractBinaryTree<K, V>.createFileHeader(): String {
        val root = this.getRoot()
        root ?: throw SerializeException()

        //соответствие названий обобщенных полей к фактическим типам
        val typeMap = mutableMapOf<String, String>()
        val typeParamNames = root::class.typeParameters.map { it.name }
        for (p in root::class.memberProperties) {
            if (p.visibility != KVisibility.PRIVATE) {
                val ignoreAnnot = p.findAnnotation<IgnoredField>()
                if (ignoreAnnot == null ||
                        (ignoreAnnot.target != IgnoreTargets.SERIALIZE &&
                                ignoreAnnot.target != IgnoreTargets.ALL)) {
                    val fieldVal = p.getter.call(root)
                    if (fieldVal != null && p.returnType.toString() in typeParamNames) {
                        typeMap += p.name to fieldVal::class.jvmName
                    }
                }
            }
        }
        var header = """
            treeType=${this::class.jvmName}
            nodeType=${root::class.jvmName}
        """.trimIndent()
        for (prop in typeMap) {
            header += "\n${prop.key}=${prop.value}"
        }
        return header
    }

    private fun AbstractBinaryNode<*, *>.serialize(nodes: ArrayList<String>): ArrayList<String> {
        var rec = "("
        val primeParams = this::class.primaryConstructor!!.parameters
        var flag = true
        //запись параметров первичного конструктора
        for (param in primeParams) {
            val prop = this::class.memberProperties.find { it.name == param.name }
            val propVal = prop!!.getter.call(this)
            if (!flag) {
                rec += ","
            } else {
                flag = false
            }
            rec += """${prop.name}="${propVal?.toTextWithType(adapterFactories)}""""
        }
        flag = true
        //запись остальных параметров
        for (p in this::class.memberProperties) {
            if (p.visibility != KVisibility.PRIVATE) {
                if (primeParams.find { it.name == p.name } == null) {
                    val ignoreAnnot = p.findAnnotation<IgnoredField>()
                    if (ignoreAnnot == null ||
                            (ignoreAnnot.target != IgnoreTargets.SERIALIZE &&
                             ignoreAnnot.target != IgnoreTargets.ALL)) {
                        val fieldValue = p.getter.call(this)
                        if (flag) {
                            rec += ";"
                            flag = false
                        } else {
                            rec += ","
                        }
                        rec += """${p.name}="${fieldValue?.toTextWithType(adapterFactories)}""""
                    }
                }
            }
        }
        rec += """|)|"${getLeft()?.key?.toTextWithType(adapterFactories)}"|"${getRight()?.key?.toTextWithType(adapterFactories)}"""".trimMargin()
        nodes += rec
        if (getLeft() != null) {
            getLeft()!!.serialize(nodes)
        }
        if (getRight() != null) {
            getRight()!!.serialize(nodes)
        }
        return nodes
    }
}
package example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.json

import com.google.gson.JsonDeserializer
import com.google.gson.JsonSerializer
import example.andrew.sdk.ExtPoint
import kotlin.reflect.KClass

interface JsonAdapterFactory<out T: Any>: ExtPoint {

    val type: KClass<*>

    fun createAdapter(): JsonAdapter<out T>
}

interface JsonAdapter<T>: JsonSerializer<T>, JsonDeserializer<T>, ExtPoint
package example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer

import example.andrew.logic.nodes.Color
import example.andrew.sdk.DefaultExtension
import example.andrew.sdk.Expandable
import javafx.scene.image.Image

@DefaultExtension
class RBColorAdapterFactory(override val parent: Expandable): TextAdapterFactory<Color> {

    override val type = Color::class

    override fun createAdapter() = RBColorAdapter(parent)
}

@DefaultExtension
class ImageAdapterFactory(override val parent: Expandable): TextAdapterFactory<Image> {

    override val type = Image::class

    override fun createAdapter() = ImageAdapter(parent)
}

@DefaultExtension
class StringAdapterFactory(override val parent: Expandable): TextAdapterFactory<String> {

    override val type = String::class

    override fun createAdapter() = StringAdapter(parent)
}

@DefaultExtension
class IntAdapterFactory(override val parent: Expandable): TextAdapterFactory<Int> {

    override val type = Int::class

    override fun createAdapter() = IntAdapter(parent)
}
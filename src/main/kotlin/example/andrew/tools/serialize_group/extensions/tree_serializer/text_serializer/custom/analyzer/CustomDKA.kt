package example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.custom.analyzer

import example.andrew.tools.serialize_group.extensions.tree_serializer.lexer.DKA
import example.andrew.tools.serialize_group.extensions.tree_serializer.lexer.DKAFactory
import example.andrew.tools.serialize_group.extensions.tree_serializer.lexer.Token
import java.lang.StringBuilder

enum class CustomTokenType {
    ID, PRIME_ID, VALUE, SEPARATOR
}

class CustomDKAFactory: DKAFactory {

    override fun createDKA(tokens: ArrayList<Token>): CustomDKA {
        val dka = CustomDKA(tokens)
        dka.generateJumpTable()
        return dka
    }
}

class CustomDKA(tokens: ArrayList<Token>): DKA() {

    private val buffer = StringBuilder("")

    override val states = mapOf(
        "A" to emptyState,
        "B" to emptyState,
        "C" to { _, sym: Char -> buffer.append(sym) },
        "D" to { _, _ -> tokens.add(Token(CustomTokenType.PRIME_ID, buffer.toString())); buffer.clear() },
        "E" to { prev: String, sym: Char -> if (prev != "D") buffer.append(sym) },
        "F" to { _, _ -> tokens.add(Token(CustomTokenType.VALUE, buffer.toString())); buffer.clear() },
        "G" to emptyState,
        "H" to { _, sym: Char -> buffer.append(sym) },
        "I" to { _, _ -> tokens.add(Token(CustomTokenType.ID, buffer.toString())); buffer.clear() },
        "K" to { prev: String, sym: Char -> if (prev != "I") buffer.append(sym) },
        "L" to { _, _ -> tokens.add(Token(CustomTokenType.VALUE, buffer.toString())); buffer.clear() },
        "M" to emptyState,
        "N" to { _, _ -> tokens.add(Token(CustomTokenType.SEPARATOR, ""))},
        "O" to { prev: String, sym: Char -> if (prev != "N") buffer.append(sym) },
        "P" to { _, _ -> tokens.add(Token(CustomTokenType.VALUE, buffer.toString())); buffer.clear() },
        "R" to emptyState,
        "S" to { prev: String, sym: Char -> if (prev != "R") buffer.append(sym) },
        "T" to { _, _ -> tokens.add(Token(CustomTokenType.VALUE, buffer.toString())); buffer.clear() }
    )

    override val startState = "A"

    override val endState = "A"

    override val jumps = mapOf(
        "(" to {sym: Char -> sym == '('},
        "w" to {sym: Char -> sym in 'a'..'z' || sym in 'A'..'Z' || sym in '0'..'9' || sym == '_'},
        "=" to {sym: Char -> sym == '='},
        "\"" to {sym: Char -> sym == '"'},
        "," to {sym: Char -> sym == ','},
        ";" to {sym: Char -> sym == ';'},
        ")" to {sym: Char -> sym == ')'},
        "|" to {sym: Char -> sym == '|'},
        "n" to {sym: Char -> sym == '\n'},
        "*" to {sym: Char -> sym != '"'}
    )

    override val jumpList = arrayListOf(
        "A" - "(" - "B",
        "B" - "w" - "C",
        "C" - "w" - "C",
        "C" - "=" - "D",
        "D" - "\"" - "E",
        "E" - "*" - "E",
        "E" - "\"" - "F",
        "F" - "," - "B",
        "F" - ";" - "G",
        "G" - "w" - "H",
        "H" - "w" - "H",
        "H" - "=" - "I",
        "I" - "\"" - "K",
        "K" - "*" - "K",
        "K" - "\"" - "L",
        "L" - "," - "G",
        "L" - ")" - "M",
        "F" - ")" - "M",
        "M" - "|" - "N",
        "N" - "\"" - "O",
        "O" - "*" - "O",
        "O" - "\"" - "P",
        "P" - "|" - "R",
        "R" - "\"" - "S",
        "S" - "*" - "S",
        "S" - "\"" - "T",
        "T" - "n" - "A"
    )
}
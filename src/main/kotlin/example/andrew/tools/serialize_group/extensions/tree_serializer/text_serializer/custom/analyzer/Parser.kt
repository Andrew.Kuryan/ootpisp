package example.andrew.tools.serialize_group.extensions.tree_serializer.text_serializer.custom.analyzer

import example.andrew.tools.serialize_group.extensions.tree_serializer.lexer.Token

data class NodeRecord(val primeProps: Map<String, String>,
                      val secProps: Map<String, String>,
                      val left: String,
                      val right: String)

class Parser {

    fun parse(tokens: List<Token>): ArrayList<NodeRecord> {
        val list = arrayListOf<NodeRecord>()
        val iterator = tokens.iterator()
        while (iterator.hasNext()) {
            val primeProps = mutableMapOf<String, String>()
            val secProps = mutableMapOf<String, String>()
            var token = iterator.next()
            while (token.type == CustomTokenType.PRIME_ID) {
                primeProps += token.value to iterator.next().value
                token = iterator.next()
            }
            while (token.type != CustomTokenType.SEPARATOR) {
                secProps += token.value to iterator.next().value
                token = iterator.next()
            }
            val left = iterator.next().value
            val right = iterator.next().value
            list += NodeRecord(primeProps, secProps, left, right)
        }
        return list
    }
}
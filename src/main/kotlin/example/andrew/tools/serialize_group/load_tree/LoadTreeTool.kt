package example.andrew.tools.serialize_group.load_tree

import example.andrew.sdk.OotpispApp
import example.andrew.sdk.Tool
import example.andrew.tools.serialize_group.InfoView
import example.andrew.tools.serialize_group.ext_points.SaveMiddleware
import example.andrew.tools.serialize_group.ext_points.TreeSerializer
import javafx.beans.property.SimpleBooleanProperty
import javafx.event.EventTarget
import javafx.stage.FileChooser
import tornadofx.*
import java.io.FileInputStream
import java.lang.Exception

class LoadTreeTool(app: OotpispApp): Tool(app = app,
            name = "Load tree",
            icoPath = "file:./src/main/resources/drawable/document-send.png",
            disableProperty = SimpleBooleanProperty(false)) {

    override fun action(source: EventTarget) {
        val saveFormats = extensions.filter { it is TreeSerializer }.map { it as TreeSerializer }
        val saveMiddles = extensions.filter { it is SaveMiddleware }.map { it as SaveMiddleware }
        val saveExtensions = saveFormats.map {
            FileChooser.ExtensionFilter(it.formatName, it.fileExtension)
        }.plus(saveMiddles.map {
            FileChooser.ExtensionFilter(it.extName, it.fileExtension)
        }).toTypedArray()

        val file = chooseFile("Choose file", saveExtensions, FileChooserMode.Single)
        if (file.isEmpty()) {
            InfoView("No file selected").openModal(block = true)
        } else {
            var parser = saveFormats.find { it.fileExtension.trimStart('*', '.') == file[0].extension }
            if (parser == null) {
                val middle = saveMiddles.find { it.fileExtension.trimStart('*', '.') == file[0].extension }
                if (middle == null) {
                    InfoView("No plugin installed").openModal(block = true)
                } else {
                    val `is` = FileInputStream(file[0].canonicalPath)
                    val(name, bytes) = middle.loadHandler(`is`.readAllBytes())

                    parser = saveFormats.find { name.endsWith(it.fileExtension.trimStart('*', '.')) }
                    try {
                        val tree = parser!!.parse(bytes)
                        `is`.close()

                        val keyType = tree.getRoot()!!.key::class
                        val valueType = tree.getRoot()!!.value::class
                        val view = app!!.treeViewFactory.createTreeView(
                                treeFactory = app.treePull.first(),
                                keyFactory = app.availableInputFactories.find {
                                    it.type == keyType } ?: app.availableInputFactories.first(),
                                valueFactory = app.availableInputFactories.find {
                                    it.type == valueType } ?: app.availableInputFactories.first())
                        view.tree = tree
                        app.editor.createNewTab(view, app.treePull.find { it.treeType == tree::class }!!.treeName)
                        app.editor.activeTreeView.repaint()
                        InfoView("Tree loaded successfully").openModal(block = true)
                    } catch (exc: Exception) {
                        exc.printStackTrace()
                        InfoView("An error occurred while loading tree").openModal(block = true)
                    }
                }
            } else {
                try {
                    val `is` = FileInputStream(file[0].canonicalPath)
                    val tree = parser.parse(`is`.readAllBytes())
                    `is`.close()

                    val keyType = tree.getRoot()!!.key::class
                    val valueType = tree.getRoot()!!.value::class
                    val view = app!!.treeViewFactory.createTreeView(
                            treeFactory = app.treePull.first(),
                            keyFactory = app.availableInputFactories.find {
                                it.type == keyType } ?: app.availableInputFactories.first(),
                            valueFactory = app.availableInputFactories.find {
                                it.type == valueType } ?: app.availableInputFactories.first())
                    view.tree = tree
                    app.editor.createNewTab(view, app.treePull.find { it.treeType == tree::class }!!.treeName)
                    app.editor.activeTreeView.repaint()
                    InfoView("Tree loaded successfully").openModal(block = true)
                } catch (exc: Exception) {
                    InfoView("An error occurred while loading tree").openModal(block = true)
                }
            }
        }
    }
}
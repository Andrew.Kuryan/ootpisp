package example.andrew.tools.serialize_group

import example.andrew.tools.tree_group.ClearDialogStyle
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import tornadofx.*

class InfoView(message: String) : View("Info dialog") {

    init {
        importStylesheet<ClearDialogStyle>()
    }

    override val root = vbox {
        addClass(ClearDialogStyle.cldlgMainPane)
        label(message) {
            addClass(ClearDialogStyle.labMessage)
        }
        hbox {
            addClass(ClearDialogStyle.dialogButtonBox)
            VBox.setVgrow(this, Priority.ALWAYS)
            button("OK") {
                setOnMouseClicked {
                    currentStage?.close()
                }
            }
        }
    }
}

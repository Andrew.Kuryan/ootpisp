package example.andrew.tools

import example.andrew.sdk.OotpispApp
import example.andrew.sdk.ToolGroup

class DefaultToolGroup(app: OotpispApp): ToolGroup(app, "Default group")
package example.andrew.logic.nodes

import example.andrew.annotations.FieldName
import example.andrew.annotations.IgnoreTargets
import example.andrew.annotations.IgnoredField
import java.io.Serializable

class AVLTreeNode<K : Comparable<K>, V: Any>(key: K, value: V) :
        BinaryTreeNode<K, V>(key, value), Serializable {

    private val serialVersionUID = 6529685098267757682L

    @FieldName("Balancing")
    @IgnoredField(IgnoreTargets.SERIALIZE)
    val balancing: Int
        get() = (getLeft()?.height ?: 0) - (getRight()?.height ?: 0)

    override fun getLeft() = getChild(0) as? AVLTreeNode<K, V>

    override fun getRight() = getChild(1) as? AVLTreeNode<K, V>

    override fun toString() = "($key, $value, $balancing)"
}
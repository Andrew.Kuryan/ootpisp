package example.andrew.logic.nodes

import example.andrew.annotations.FieldName
import java.io.Serializable

enum class Color {
    RED, BLACK;

    operator fun not(): Color =
            when (this) {
                RED -> BLACK
                BLACK -> RED
            }

    override fun toString() = when (this) {
        RED -> "Red"
        BLACK -> "Black"
    }
}

class RBTreeNode<K : Comparable<K>, V: Any>(key: K,
                                       value: V,
                                       @FieldName("Color")
                                       var color: Color) :
        BinaryTreeNode<K, V>(key, value), Serializable {

    private val serialVersionUID = 6529685098267757683L

    override fun getLeft() = getChild(0) as? RBTreeNode<K, V>

    override fun getRight() = getChild(1) as? RBTreeNode<K, V>

    override fun toString() = "($key, $value, $color)"
}
package example.andrew.logic.nodes

import example.andrew.annotations.IgnoredField
import example.andrew.annotations.FieldName
import example.andrew.annotations.IgnoreTargets
import example.andrew.sdk.trees.AbstractBinaryNode
import java.io.Serializable

class TooMuchChildrenException : Exception()

open class BinaryTreeNode<K : Comparable<K>, V: Any>(@IgnoredField(IgnoreTargets.VIEW)
                                                override var key: K,
                                                value: V):
        TreeNode<V>(value), Serializable, AbstractBinaryNode<K, V> {

    private val serialVersionUID = 6529685098267757681L

    @FieldName("Height")
    @IgnoredField(IgnoreTargets.SERIALIZE)
    override val height: Int
        get() = Math.max(getLeft()?.height ?: 0, getRight()?.height ?: 0) + 1

    override operator fun compareTo(node: AbstractBinaryNode<K, V>) = key.compareTo(node.key)

    override fun equals(other: Any?) =
            if (other is BinaryTreeNode<*, *>) {
                other.key == key
            } else {
                false
            }

    override fun addChild(child: TreeNode<V>) {
        if (children.size == 2) {
            throw TooMuchChildrenException()
        } else {
            super.addChild(child)
        }
    }

    override fun setChild(child: TreeNode<V>?, index: Int) {
        if (index > 1) {
            throw TooMuchChildrenException()
        } else {
            super.setChild(child, index)
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun getLeft() = getChild(0) as? BinaryTreeNode<K, V>

    @Suppress("UNCHECKED_CAST")
    override fun getRight() = getChild(1) as? BinaryTreeNode<K, V>

    override fun toString() = "($key, $value)"
}
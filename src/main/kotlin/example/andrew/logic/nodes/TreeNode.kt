package example.andrew.logic.nodes

import example.andrew.annotations.IgnoredField
import example.andrew.annotations.FieldName
import example.andrew.annotations.IgnoreTargets
import java.io.Serializable

open class TreeNode<V>(@IgnoredField(IgnoreTargets.VIEW)
                       var value: V): Serializable {

    private val serialVersionUID = 6529685098267757680L

    @FieldName("Children")
    @IgnoredField(IgnoreTargets.SERIALIZE)
    val children = arrayListOf<TreeNode<V>?>()

    open fun addChild(child: TreeNode<V>) {
        children += child
    }

    open fun setChild(child: TreeNode<V>?, index: Int) {
        if (children.size <= index) {
            for (i in 0..(index - children.size)) {
                children.add(null)
            }
        }
        children[index] = child
    }

    fun getChild(index: Int) = try {
        children[index]
    } catch (exc: IndexOutOfBoundsException) {
        null
    }
}
package example.andrew.logic.trees

import example.andrew.logic.nodes.BinaryTreeNode
import example.andrew.sdk.trees.AbstractBinaryNode
import example.andrew.sdk.trees.AbstractBinaryTree
import example.andrew.sdk.trees.AbstractBinaryTreeFactory
import example.andrew.sdk.trees.ExistingKeyException
import example.andrew.sdk.trees.NoSuchKeyException
import java.io.Serializable
import kotlin.reflect.KClass

const val LEFT = 0
const val RIGHT = 1

open class BinaryTreeFactory: AbstractBinaryTreeFactory {

    override val treeType: KClass<*> = BinaryTree::class

    override val treeName = "Binary tree"

    override fun createTree(key: Comparable<Any>, value: Any) = BinaryTree(key, value)

    override fun createTreeNode(key: Comparable<Any>, value: Any) = BinaryTreeNode(key, value)
}

open class BinaryTree<K : Comparable<K>, V: Any>(): Tree<V>(),
        AbstractBinaryTree<K, V>, Serializable {

    private val serialVersionUID = 6529685098267757691L

    constructor(key: K, value: V) : this() {
        root = BinaryTreeNode(key, value)
    }

    constructor(root: BinaryTreeNode<K, V>): this() {
        this.root = root
    }

    override fun getRoot() = root as BinaryTreeNode<K, V>?

    /**API functions**/

    override fun insert(key: K, value: V) {
        root = getRoot()?.insert(BinaryTreeNode(key, value)) ?: BinaryTreeNode(key, value)
    }

    override fun remove(key: K) {
        find(key)
        root = getRoot().remove(key)
    }

    override fun find(key: K) = getRoot()?.find(key) ?: throw NoSuchKeyException(key)

    override fun clear() {
        root = null
        System.gc()
    }

    override fun getHeight(): Int {
        var max = 0
        for (node in RAB()) {
            if (node.height > max) {
                max = node.height
            }
        }
        return max
    }

    override infix fun levelOf(node: AbstractBinaryNode<*, *>): Int {
        return getRoot()?.recursiveLevel(0, node) ?: -1
    }

    override fun RAB(): ArrayList<AbstractBinaryNode<*, *>> {
        val list = ArrayList<AbstractBinaryNode<*, *>>()
        getRoot().recursiveRAB(list)
        return list
    }

    /**Recursive functions**/

    private fun BinaryTreeNode<*, *>?.recursiveLevel(level: Int, node: AbstractBinaryNode<*, *>): Int {
        if (this == null) {
            return -1
        } else if (this.key == node.key) {
            return level
        } else {
            return Math.max(getLeft().recursiveLevel(level + 1, node),
                    getRight().recursiveLevel(level + 1, node))
        }
    }

    private fun AbstractBinaryNode<*, *>?.recursiveRAB(list: ArrayList<AbstractBinaryNode<*, *>>) {
        if (this != null) {
            list.add(this)
            getLeft().recursiveRAB(list)
            getRight().recursiveRAB(list)
        }
    }

    open fun BinaryTreeNode<K, V>.insertHandler(node: BinaryTreeNode<K, V>): BinaryTreeNode<K, V> = this

    open fun BinaryTreeNode<K, V>.deleteHandler(): BinaryTreeNode<K, V> = this

    open fun BinaryTreeNode<K, V>?.remove(key: K): BinaryTreeNode<K, V>? {
        when {
            this == null -> return this
            key < this.key -> setChild(getLeft().remove(key), LEFT)
            key > this.key -> setChild(getRight().remove(key), RIGHT)
            else -> {
                val (parent, side) = getRoot()!!.findParent(key)
                if (getLeft() == null && getRight() == null) {
                    return null
                } else if (getLeft() == null) {
                    val temp = getRight()
                    parent?.setChild(null, side)
                    return temp
                } else if (getRight() == null) {
                    val temp = getLeft()
                    parent?.setChild(null, side)
                    return temp
                }
                val temp = getPredecessor(getLeft()!!)
                this.value = temp.value
                this.key = temp.key
                setChild(getLeft().remove(temp.key), LEFT)
            }
        }
        return deleteHandler()
    }

    private fun BinaryTreeNode<K, V>.find(key: K): BinaryTreeNode<K, V>? =
            when {
                key < this.key -> getLeft()?.find(key)
                key > this.key -> getRight()?.find(key)
                else -> this
            }

    protected fun BinaryTreeNode<K, V>.insert(node: BinaryTreeNode<K, V>): BinaryTreeNode<K, V> {
        when {
            node < this -> {
                //node.level = this.level+1
                setChild(getLeft()?.insert(node) ?: node, LEFT)
            }
            node > this -> {
                //node.level = this.level+1
                setChild(getRight()?.insert(node) ?: node, RIGHT)
            }
            else -> throw ExistingKeyException(node.key)
        }
        return insertHandler(node)
    }

    data class ParentNode<K : Comparable<K>, V: Any>(val parent: BinaryTreeNode<K, V>?, val side: Int)

    private fun BinaryTreeNode<K, V>.findParent(key: K): ParentNode<K, V> =
            when {
                key == this.getLeft()?.key -> ParentNode(this, LEFT)
                key == this.getRight()?.key -> ParentNode(this, RIGHT)
                key < this.key -> getLeft()?.findParent(key) ?: ParentNode(null, -1)
                else -> getRight()?.findParent(key) ?: ParentNode(null, -1)
            }

    /**Support functions**/

    /*получение наименьшего элемента в поддереве*/
    protected fun getSuccessor(node: BinaryTreeNode<K, V>): BinaryTreeNode<K, V> =
            if (node.getLeft() == null)
                node
            else
                getSuccessor(node.getLeft()!!)

    /*получение наибольшего элемента в поддереве*/
    private fun getPredecessor(node: BinaryTreeNode<K, V>): BinaryTreeNode<K, V> =
            if (node.getRight() == null)
                node
            else
                getPredecessor(node.getRight()!!)

    override fun toString() = getRoot()?.output(0) ?: "Tree is empty"

    private fun BinaryTreeNode<*, *>?.output(deep: Int): String {
        val str = StringBuilder()
        if (this != null) {
            str.append(this.getRight().output(deep + 1))
            for (i in 0 until deep) {
                str.append("\t")
            }
            str.append(this.toString()).append("\n")
            str.append(this.getLeft().output(deep + 1))
        }
        return str.toString()
    }
}
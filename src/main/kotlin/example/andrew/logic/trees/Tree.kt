package example.andrew.logic.trees

import example.andrew.logic.nodes.TreeNode
import java.io.Serializable

abstract class Tree<V: Any>: Serializable {

    private val serialVersionUID = 6529685098267757690L

    var root: TreeNode<V>? = null
}
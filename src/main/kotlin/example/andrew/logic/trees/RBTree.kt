package example.andrew.logic.trees

import example.andrew.logic.nodes.BinaryTreeNode
import example.andrew.logic.nodes.Color
import example.andrew.logic.nodes.RBTreeNode
import java.io.Serializable

class RBTreeFactory : BinaryTreeFactory() {

    override val treeType = RBTree::class

    override val treeName = "Red-black tree"

    override fun createTree(key: Comparable<Any>, value: Any) = RBTree(key, value)

    override fun createTreeNode(key: Comparable<Any>, value: Any) = RBTreeNode(key, value, Color.BLACK)
}

class RBTree<K : Comparable<K>, V: Any>() : BinaryTree<K, V>(), Serializable {

    private val serialVersionUID = 6529685098267757693L

    constructor(key: K, value: V) : this() {
        root = RBTreeNode(key, value, Color.BLACK)
    }

    override fun getRoot() = root as? RBTreeNode<K, V>?

    /**API functions*/

    override fun insert(key: K, value: V) {
        root = getRoot()?.insert(RBTreeNode(key, value, Color.RED)) ?: RBTreeNode(key, value, Color.RED)
        getRoot()?.color = Color.BLACK
    }

    override fun remove(key: K) {
        find(key)
        if (!getRoot()?.getLeft().isRed() && !getRoot()?.getRight().isRed()) {
            getRoot()?.color = Color.RED
        }
        root = getRoot()?.remove(key)
        getRoot()?.color = Color.BLACK
    }

    /**Recursive functions*/

    override fun BinaryTreeNode<K, V>.insertHandler(node: BinaryTreeNode<K, V>) =
            balance(this as RBTreeNode<K, V>)

    private fun RBTreeNode<K, V>.remove(key: K): RBTreeNode<K, V>? {
        var h = this
        if (key < h.key) {
            if (!h.getLeft().isRed() && !h.getLeft()!!.getLeft().isRed())
                h = moveRedLeft(h)
            h.setChild(h.getLeft()!!.remove(key), LEFT)
        } else {
            if (h.getLeft().isRed())
                h = rotateRight(h)
            if (key.compareTo(h.key) == 0 && (h.getRight() == null))
                return null
            if (!h.getRight().isRed() && !h.getRight()?.getLeft().isRed())
                h = moveRedRight(h)
            if (key.compareTo(h.key) == 0) {
                val x = getSuccessor(h.getRight()!!)
                h.key = x.key
                h.value = x.value
                h.setChild(deleteMin(h.getRight()!!), RIGHT)
            } else
                h.setChild(h.getRight()!!.remove(key), RIGHT)
        }
        return balance(h)
    }

    /**Support functions**/

    private fun deleteMin(node: RBTreeNode<K, V>): RBTreeNode<K, V>? {
        var h = node
        if (h.getLeft() == null)
            return null
        if (!h.getLeft().isRed() && !h.getLeft()!!.getLeft().isRed())
            h = moveRedLeft(h)
        h.setChild(deleteMin(h.getLeft()!!), LEFT)
        return balance(h)
    }

    private fun balance(node: RBTreeNode<K, V>): RBTreeNode<K, V> {
        var h = node
        if (h.getRight().isRed() && !h.getLeft().isRed())
            h = rotateLeft(h)
        if (h.getLeft().isRed() && h.getLeft()!!.getLeft().isRed())
            h = rotateRight(h)
        if (h.getLeft().isRed() && h.getRight().isRed())
            flipColors(h)
        return h
    }

    private fun moveRedLeft(node: RBTreeNode<K, V>): RBTreeNode<K, V> {
        var h = node
        flipColors(h)
        if (h.getRight()?.getLeft().isRed()) {
            h.setChild(rotateRight(h.getRight()!!), RIGHT)
            h = rotateLeft(h)
            flipColors(h)
        }
        return h
    }

    private fun moveRedRight(node: RBTreeNode<K, V>): RBTreeNode<K, V> {
        var h = node
        flipColors(h)
        if (h.getLeft()?.getLeft().isRed()) {
            h = rotateRight(h)
            flipColors(h)
        }
        return h
    }

    private fun RBTreeNode<K, V>?.isRed() = (this?.color ?: Color.BLACK) == Color.RED

    private fun rotateRight(parent: RBTreeNode<K, V>): RBTreeNode<K, V> {
        val node = parent.getLeft()
        parent.setChild(node!!.getRight(), LEFT)
        node.setChild(parent, RIGHT)
        node.color = node.getRight()!!.color
        node.getRight()!!.color = Color.RED
        return node
    }

    private fun rotateLeft(parent: RBTreeNode<K, V>): RBTreeNode<K, V> {
        val node = parent.getRight()
        parent.setChild(node!!.getLeft(), RIGHT)
        node.setChild(parent, LEFT)
        node.color = node.getLeft()!!.color
        node.getLeft()!!.color = Color.RED
        return node
    }

    private fun flipColors(h: RBTreeNode<K, V>) {
        h.color = !h.color
        h.getLeft()!!.color = !h.getLeft()!!.color
        h.getRight()!!.color = !h.getRight()!!.color
    }
}
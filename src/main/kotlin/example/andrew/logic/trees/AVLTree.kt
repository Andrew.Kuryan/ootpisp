package example.andrew.logic.trees

import example.andrew.logic.nodes.AVLTreeNode
import example.andrew.logic.nodes.BinaryTreeNode
import java.io.Serializable

class AVLTreeFactory : BinaryTreeFactory() {

    override val treeType = AVLTree::class

    override val treeName = "AVL Tree"

    override fun createTree(key: Comparable<Any>, value: Any) = AVLTree(key, value)

    override fun createTreeNode(key: Comparable<Any>, value: Any) = AVLTreeNode(key, value)
}

class AVLTree<K : Comparable<K>, V: Any>() : BinaryTree<K, V>(), Serializable {

    private val serialVersionUID = 6529685098267757692L

    constructor(key: K, value: V) : this() {
        root = AVLTreeNode(key, value)
    }

    override fun getRoot() = root as? AVLTreeNode<K, V>?

    /**API functions*/

    override fun insert(key: K, value: V) {
        root = getRoot()?.insert(AVLTreeNode(key, value)) ?: AVLTreeNode(key, value)
    }

    /**Recursive functions**/

    override fun BinaryTreeNode<K, V>.insertHandler(node: BinaryTreeNode<K, V>) =
            (this as AVLTreeNode<K, V>).checkBalance(node as AVLTreeNode<K, V>)

    override fun BinaryTreeNode<K, V>.deleteHandler() =
            (this as AVLTreeNode<K, V>).checkBalanceAndRotate()

    /**Support functions**/

    private fun AVLTreeNode<K, V>.checkBalance(node: AVLTreeNode<K, V>): AVLTreeNode<K, V> =
            when {
                balancing > 1 && node < getLeft()!! -> rightRotation(this)
                balancing < -1 && node > getRight()!! -> leftRotation(this)
                balancing > 1 && node > getLeft()!! -> {
                    setChild(leftRotation(getLeft() as AVLTreeNode<K, V>), LEFT)
                    rightRotation(this)
                }
                balancing < -1 && node < getRight()!! -> {
                    setChild(rightRotation(getRight() as AVLTreeNode<K, V>), RIGHT)
                    leftRotation(this)
                }
                else -> this
            }

    private fun AVLTreeNode<K, V>.checkBalanceAndRotate(): AVLTreeNode<K, V> =
            when {
                balancing > 1 -> {
                    if (getLeft()!!.balancing < 0) {
                        setChild(leftRotation(getLeft()!!), LEFT)
                    }
                    rightRotation(this)
                }
                balancing < -1 -> {
                    if (getRight()!!.balancing > 0) {
                        setChild(rightRotation(getRight()!!), RIGHT)
                    }
                    leftRotation(this)
                }
                else -> this
            }

    private fun leftRotation(node: AVLTreeNode<K, V>): AVLTreeNode<K, V> {
        val newParentNode = node.getRight()
        val mid = newParentNode!!.getLeft()
        newParentNode.setChild(node, LEFT)
        node.setChild(mid, RIGHT)
        return newParentNode
    }

    private fun rightRotation(node: AVLTreeNode<K, V>): AVLTreeNode<K, V> {
        val newParentNode = node.getLeft()
        val mid = newParentNode!!.getRight()
        newParentNode.setChild(node, RIGHT)
        node.setChild(mid, LEFT)
        return newParentNode
    }
}
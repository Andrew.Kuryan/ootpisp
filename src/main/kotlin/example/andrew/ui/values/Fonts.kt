package example.andrew.ui.values

import javafx.scene.paint.Paint
import javafx.scene.text.Font
import javafx.scene.text.FontPosture
import javafx.scene.text.FontWeight
import tornadofx.*

data class LabelFont(val font: Font, val color: Paint)

fun CssSelectionBlock.labelFont(lFont: LabelFont) {
    this.font = lFont.font
    this.textFill = lFont.color
}

object Fonts {
    val accelFont = LabelFont(
            Font.font("System", FontWeight.NORMAL, FontPosture.REGULAR, 22.0),
            Paint.valueOf("#1111AA"))
    val descrFont = LabelFont(
            Font.font("System", FontWeight.NORMAL, FontPosture.REGULAR, 22.0),
            Paint.valueOf("#AAAAAA"))

    val cdTitleFont = LabelFont(
            Font.font("System", FontWeight.EXTRA_BOLD, FontPosture.ITALIC, 22.0),
            Paint.valueOf("#FFFFFF"))
    val cdParagraphFont = LabelFont(
            Font.font("System", FontWeight.BOLD, FontPosture.REGULAR, 15.0),
            Paint.valueOf("#FFFFFF"))
    val cdItemFont = LabelFont(
            Font.font("System", FontWeight.NORMAL, FontPosture.REGULAR, 14.0),
            Paint.valueOf("#FFFFFF"))

    val adItemFont = LabelFont(
            Font.font("System", FontWeight.NORMAL, FontPosture.REGULAR, 12.0),
            Paint.valueOf("#FFFFFF"))

    val tfInputFont = LabelFont(
            Font.font("System", FontWeight.NORMAL, FontPosture.REGULAR, 11.0),
            Paint.valueOf("#FFFFFF"))
    val simpleButtonFont = LabelFont(
            Font.font("System", FontWeight.NORMAL, FontPosture.REGULAR, 10.0),
            Paint.valueOf("#FFFFFF"))

    val nodeFont = LabelFont(
            Font.font("System", FontWeight.BOLD, FontPosture.REGULAR, 14.0),
            Paint.valueOf("#FFFFFF"))

    val errorFont = LabelFont(
            Font.font("System", FontWeight.NORMAL, FontPosture.REGULAR, 10.0),
            Paint.valueOf("#FF0000"))
}
package example.andrew.ui.values

import tornadofx.*

object Colors {
    val mainBorderColor = c(50, 50, 50)
    val backColor = c(55, 55, 55)
    val barColor = c(100, 100, 100)
    val controlColor = c(70, 70, 70)
    val cdBackColor = c(55, 55, 55, 0.5)
}
package example.andrew.ui.styles

import example.andrew.ui.values.Colors.backColor
import example.andrew.ui.values.Colors.barColor
import example.andrew.ui.values.Colors.mainBorderColor
import javafx.scene.paint.Color
import tornadofx.*

class TabPaneStyle : Stylesheet() {

    init {
        tabPane {
            tabHeaderBackground {
                backgroundColor += barColor
            }
            headersRegion {
                menuDownArrow {
                    backgroundColor += barColor
                }
                tab and selected {
                    borderWidth += box(0.px, 0.px, 3.px, 0.px)
                    borderColor += box(Color.POWDERBLUE)
                }
                tab {
                    backgroundColor += mainBorderColor
                    borderRadius += box(0.px)
                    borderWidth += box(0.px, 0.px, 3.px, 0.px)
                    borderColor += box(mainBorderColor)
                }
                focusColor = Color.TRANSPARENT
                faintFocusColor = Color.TRANSPARENT
            }
            tabLabel {
                textFill = c(255, 255, 255)
            }
            backgroundColor += backColor
        }
    }
}
package example.andrew.ui.styles

import example.andrew.Main
import example.andrew.ui.values.Colors
import javafx.geometry.Pos
import javafx.scene.paint.Color
import tornadofx.*

class TreeStyle : Stylesheet() {

    companion object {
        const val hGap = 20.0
        const val vGap = 50.0

        val treePane by cssclass()
        val edgePane by cssclass()
        val treeBack by cssclass()
        val link by cssclass()
    }

    init {
        treePane {
            backgroundColor += Colors.backColor
        }
        edgePane {
            backgroundColor += Color.TRANSPARENT
        }
        treeBack {
            alignment = Pos.TOP_CENTER
            padding = box(20.px)
            backgroundColor += Colors.backColor
            prefWidth = (Main.windowWidth - MainStyle.controlWidth -/*погрешность scrollpane*/9).px
            prefHeight = (Main.windowHeight - MainStyle.toolbarHeight * 2 -/*погрешность scrollpane*/23).px
        }
        link {
            stroke = Color.POWDERBLUE
            strokeWidth = 3.px
        }
    }
}
package example.andrew.ui.styles

import example.andrew.ui.values.Colors
import example.andrew.ui.values.Colors.barColor
import example.andrew.ui.values.Colors.controlColor
import example.andrew.ui.values.Colors.mainBorderColor
import example.andrew.ui.values.Fonts
import example.andrew.ui.values.labelFont
import javafx.geometry.Pos
import javafx.scene.layout.BackgroundPosition
import javafx.scene.layout.BackgroundRepeat
import javafx.scene.layout.BackgroundSize
import javafx.scene.paint.Color
import tornadofx.*
import java.net.URI

class MainStyle : Stylesheet() {

    companion object {
        const val toolbarHeight = 42.0
        const val controlWidth = 35.0
        const val toolSize = 30

        val controlBar by cssclass()
        val infoBar by cssclass()
        val labAccel by cssclass()
        val labDescr by cssclass()

        val newTabButton by cssclass()
        val buttonAdd by cssclass()
        val buttonRemove by cssclass()
        val buttonFind by cssclass()
        val buttonClear by cssclass()
        val buttonSave by cssclass()
        val buttonLoad by cssclass()

        val buttonFirstTest by cssclass()
        val buttonSecondTest by cssclass()
        val buttonThirdTest by cssclass()
    }

    init {
        root {
            backgroundColor += mainBorderColor
            borderColor += box(mainBorderColor, mainBorderColor, mainBorderColor, mainBorderColor)
        }
        labAccel and label{
            labelFont(Fonts.accelFont)
        }
        labDescr and label {
            labelFont(Fonts.descrFont)
        }
        controlBar {
            padding = box(5.px, 5.px, 0.px, 5.px)
            prefWidth = controlWidth.px
            backgroundColor += controlColor
            button {
                alignment = Pos.TOP_CENTER
                prefWidth = toolSize.px
                prefHeight = toolSize.px
                backgroundRepeat += BackgroundRepeat.NO_REPEAT to BackgroundRepeat.NO_REPEAT
                backgroundPosition += BackgroundPosition.CENTER
                backgroundSize += BackgroundSize(toolSize.toDouble() - 4.0, toolSize.toDouble() - 4.0,
                        false, false, false, false)
            }
            newTabButton and button {
                backgroundImage += URI.create("file:./src/main/resources/drawable/tab-new.png")
            }
            buttonAdd and button {
                backgroundImage += URI.create("file:./src/main/resources/drawable/list-add.png")
            }
            buttonRemove and button {
                backgroundImage += URI.create("file:./src/main/resources/drawable/list-remove.png")
            }
            buttonFind and button {
                backgroundImage += URI.create("file:./src/main/resources/drawable/edit-find.png")
            }
            buttonClear and button {
                backgroundImage += URI.create("file:./src/main/resources/drawable/edit-delete.png")
            }
            buttonSave and button {
                backgroundImage += URI.create("file:./src/main/resources/drawable/document-save-as.png")
            }
            buttonLoad and button {
                backgroundImage += URI.create("file:./src/main/resources/drawable/document-send.png")
            }

            buttonFirstTest and button {
                backgroundImage += URI.create("file:./src/main/resources/drawable/test-string.png")
            }
            buttonSecondTest and button {
                backgroundImage += URI.create("file:./src/main/resources/drawable/test-string-array.png")
            }
            buttonThirdTest and button {
                backgroundImage += URI.create("file:./src/main/resources/drawable/test-images.png")
            }
        }

        infoBar and toolBar {
            prefHeight = toolbarHeight.px
            backgroundColor += barColor
        }

        /**Common components**/

        button {
            backgroundColor += Colors.mainBorderColor
            labelFont(Fonts.adItemFont)
        }

        listView {
            minHeight = 60.px
            cell {
                backgroundColor += Colors.backColor
                prefHeight = 25.px
            }
        }

        s(scrollPane, listView) {
            borderColor += box(Color.TRANSPARENT)
            borderWidth += box(0.px)
            focusColor = Color.TRANSPARENT
            faintFocusColor = Color.TRANSPARENT
            backgroundColor += Colors.backColor
            corner {
                backgroundColor += Colors.backColor
            }
            scrollBar {
                track {
                    backgroundColor += Colors.backColor
                    borderColor += box(Color.TRANSPARENT)
                    backgroundRadius += box(0.em)
                    borderRadius += box(2.em)
                }
                s(incrementButton, decrementButton) {
                    backgroundRadius += box(0.em)
                }
                s(incrementArrow, decrementArrow) {
                    shape = " "
                    padding = box(0.px)
                }
                thumb {
                    backgroundColor += c(10, 10, 10, 0.9)
                    backgroundRadius += box(2.em)
                }
            }
            scrollBar and vertical {
                s(incrementButton, decrementButton) {
                    padding = box(0.px, 10.px, 0.px, 0.px)
                }
            }
            scrollBar and horizontal {
                s(incrementButton, decrementButton) {
                    padding = box(0.px, 0.px, 10.px, 0.px)
                }
            }
        }
    }
}
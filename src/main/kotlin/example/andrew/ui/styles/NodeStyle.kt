package example.andrew.ui.styles

import example.andrew.ui.values.Colors
import example.andrew.ui.values.Fonts
import example.andrew.ui.values.labelFont
import javafx.geometry.Pos
import javafx.scene.paint.Color
import javafx.scene.text.TextAlignment
import tornadofx.*

class NodeStyle : Stylesheet() {

    companion object {
        const val nodeWidth = 100.0
        const val nodeHeight = 70.0

        val nodePane by cssclass()
    }

    init {
        nodePane {
            alignment = Pos.CENTER
            prefWidth = nodeWidth.px; prefHeight = nodeHeight.px
            maxWidth = nodeWidth.px; maxHeight = nodeHeight.px
            backgroundColor += Colors.mainBorderColor
            borderWidth += box(1.px)
            borderRadius += box(8.px)
            borderColor += box(Color.WHITE)
            label {
                labelFont(Fonts.nodeFont)
                alignment = Pos.CENTER
                textAlignment = TextAlignment.CENTER
                maxWidth = (nodeWidth - 10).px; maxHeight = (nodeHeight - 10).px
                prefWidth = (nodeWidth - 10).px; prefHeight = (nodeHeight - 10).px
                wrapText = true
            }
        }
    }
}
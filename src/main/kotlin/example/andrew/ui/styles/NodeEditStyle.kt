package example.andrew.ui.styles

import example.andrew.ui.values.Colors
import example.andrew.ui.values.Fonts
import example.andrew.ui.values.labelFont
import javafx.geometry.Pos
import javafx.scene.layout.BackgroundPosition
import javafx.scene.layout.BackgroundRepeat
import javafx.scene.layout.BackgroundSize
import javafx.scene.text.TextAlignment
import tornadofx.*
import java.net.URI

class NodeEditStyle : Stylesheet() {

    companion object {
        val neMainPane by cssclass()
        val butContainer by cssclass()
        val butDialogContainer by cssclass()
        val labError by cssclass()
        val labHeader by cssclass()

        const val mainHeight = 380
    }

    init {
        val mainWidth = 300
        radioButton {
            radio {
                focusColor = Colors.mainBorderColor
                faintFocusColor = Colors.mainBorderColor
                backgroundRadius += box(0.px)
                backgroundColor += Colors.mainBorderColor
                prefHeight = 12.px; prefWidth = 12.px
                backgroundColor += Colors.mainBorderColor
            }
            labelFont(Fonts.adItemFont)
        }
        radioButton and selected {
            dot {
                backgroundColor += Colors.mainBorderColor
                backgroundRepeat += BackgroundRepeat.NO_REPEAT to BackgroundRepeat.NO_REPEAT
                backgroundPosition += BackgroundPosition.CENTER
                backgroundSize += BackgroundSize(12.0, 12.0,
                        false, false, false, false)
                backgroundRadius += box(0.px)
                backgroundImage += URI.create("file:./src/main/resources/drawable/object-select.png")
            }
        }
        neMainPane {
            alignment = Pos.TOP_LEFT
            prefHeight = mainHeight.px; prefWidth = mainWidth.px
            backgroundColor += Colors.controlColor
            padding = box(10.px, 15.px, 10.px, 15.px)
            spacing = 10.px
            label {
                labelFont(Fonts.adItemFont)
            }
            butContainer {
                val butWidth = 150
                padding = box(0.px, ((mainWidth - butWidth) / 2).px, 0.px, ((mainWidth - butWidth) / 2).px)
                button {
                    prefWidth = butWidth.px; prefHeight = 30.px
                    alignment = Pos.CENTER
                    labelFont(Fonts.adItemFont)
                }
            }
            butDialogContainer {
                spacing = 10.px
                alignment = Pos.CENTER_RIGHT
                button {
                    prefWidth = 100.px
                    prefHeight = 30.px
                }
            }
        }
        labError and label {
            prefHeight = 25.px
            labelFont(Fonts.errorFont)
            wrapText = true
            textAlignment = TextAlignment.CENTER
        }
        labHeader and label {
            labelFont(Fonts.cdParagraphFont)
        }
    }
}
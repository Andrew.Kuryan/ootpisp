package example.andrew.ui.views

import example.andrew.sdk.views.AbstractEditor
import example.andrew.sdk.views.AbstractTreeView
import example.andrew.sdk.views.CreateFormResult
import example.andrew.sdk.OotpispApp
import example.andrew.sdk.findTool
import example.andrew.sdk.findToolGroup
import example.andrew.tools.app_group.AppToolGroup
import example.andrew.tools.app_group.new_tab.NewTabTool
import example.andrew.tools.serialize_group.SerializeToolGroup
import example.andrew.tools.serialize_group.load_tree.LoadTreeTool
import example.andrew.tools.tree_group.TreeToolGroup
import example.andrew.tools.tree_group.clear_tree.ClearTreeTool
import example.andrew.ui.styles.MainStyle
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.value.ObservableValue
import javafx.collections.ListChangeListener
import javafx.geometry.Pos
import javafx.scene.control.Tab
import javafx.scene.control.TabPane
import javafx.scene.input.KeyCode
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import tornadofx.*

class EditorController(private val app: OotpispApp, private val tabPane: TabPane) {

    private val trees = mutableMapOf<Tab, AbstractTreeView>()
    private var activeTree: String? = null
    val treeTabProperty = true.toProperty()

    val activeTreeView: AbstractTreeView
        get() = trees.getValue(trees.keys.find { it.text == activeTree }!!)

    fun closeActiveTab() {
        val tab = trees.keys.find { it.text == activeTree }!!
        trees.remove(tab)
        tab.close()
    }

    fun createTree(result: CreateFormResult) {
        val view = app.treeViewFactory.createTreeView(
                treeFactory = result.treeType,
                keyFactory = result.keyType,
                valueFactory = result.valueType)
        createNewTab(view, result.treeType.treeName)
    }

    fun createNewTab(view: AbstractTreeView, treeName: String) {
        val newTab = Tab()
        newTab.content = view.root
        var index = 0
        for (t in tabPane.tabs) {
            if (t.text == treeName) {
                index++
            }
        }
        val id = "$treeName${if (index != 0) {
            " (${index + 1})"
        } else { "" }}"
        newTab.text = id
        newTab.setOnCloseRequest {
            it.consume()
            app.findToolGroup<TreeToolGroup>()?.
                    findTool<ClearTreeTool>()?.action(newTab)
        }
        tabPane.tabs.add(newTab)
        tabPane.selectionModel.select(newTab)
        trees += newTab to view
    }

    fun changeTabListener(observable: ObservableValue<*>, oldTab: Tab?, newTab: Tab?) {
        treeTabProperty.value = if (newTab?.content?.id != "Tree View") {
            activeTree = null
            true
        } else {
            activeTree = newTab.text
            false
        }
    }
}

class Editor(private val mApp: OotpispApp): View("Editor"), AbstractEditor {

    lateinit var tabPane: TabPane

    private val keyCombinations = arrayListOf(
        KeyCombination("New tree", KeyCode.CONTROL, KeyCode.N)
            { mApp.findToolGroup<AppToolGroup>()
                    ?.findTool<NewTabTool>()?.action(tabPane) },
        KeyCombination("Load tree", KeyCode.CONTROL, KeyCode.O)
            { mApp.findToolGroup<SerializeToolGroup>()
                    ?.findTool<LoadTreeTool>()?.action(tabPane) }
    )

    override val root = vbox {
        anchorpane {
            VBox.setVgrow(this, Priority.ALWAYS)
            tabPane = tabpane {
                tabClosingPolicy = TabPane.TabClosingPolicy.ALL_TABS
                tabs.addListener(fun(c: ListChangeListener.Change<*>?) {
                    if (c?.list?.size == 0) {
                        val page = StartupPage(keyCombinations)
                        tab(page) {
                            text = "Startup Page"
                        }
                    }
                })
                val page = StartupPage(keyCombinations)
                tab(page) {
                    text = "Startup Page"
                }
                anchorpaneConstraints {
                    topAnchor = 0.0
                    rightAnchor = 0.0
                    bottomAnchor = 0.0
                    leftAnchor = 0.0
                }
            }
            tabPane.setOnKeyPressed{
                for (keyComb in keyCombinations) {
                    keyComb.notifyKeyPressed(it.code)
                }
            }
            tabPane.setOnKeyReleased{
                for (keyComb in keyCombinations) {
                    keyComb.notifyKeyReleased(it.code)
                }
            }
        }
        toolbar {
            addClass(MainStyle.infoBar)
        }
    }

    private val controller = EditorController(mApp, tabPane)

    init {
        tabPane.selectionModel.selectedItemProperty().addListener(controller::changeTabListener)
    }

    override val activeTreeView
        get() = controller.activeTreeView

    override val treeTabProperty
        get() = controller.treeTabProperty

    override fun closeActiveTab() = controller.closeActiveTab()

    override fun createTree(result: CreateFormResult) = controller.createTree(result)

    override fun createNewTab(view: AbstractTreeView, treeName: String) =
            controller.createNewTab(view, treeName)
}

class StartupPage(private val keyCombinations: ArrayList<KeyCombination>):
        View("Startup Page") {

    override val root = anchorpane {
        vbox {
            alignment = Pos.CENTER
            spacing = 20.0
            anchorpaneConstraints {
                topAnchor = 0.0
                rightAnchor = 0.0
                bottomAnchor = 0.0
                leftAnchor = 0.0
            }
            for (keyComb in keyCombinations) {
                hbox {
                    alignment = Pos.CENTER
                    spacing = 15.0
                    label("${keyComb.keyCode1.getName()}+${keyComb.keyCode2.getName()}") {
                        addClass(MainStyle.labAccel)
                    }
                    label(keyComb.description) {
                        addClass(MainStyle.labDescr)
                    }
                }
            }
        }
    }
}

class KeyCombination(val description: String,
                     val keyCode1: KeyCode,
                     val keyCode2: KeyCode,
                     private val action: () -> Unit) {

    private val firstPressed = SimpleBooleanProperty()
    private val secondPressed = SimpleBooleanProperty()
    private val bothPressed = firstPressed.and(secondPressed)

    fun notifyKeyPressed(keyCode: KeyCode) {
        if (keyCode == keyCode1) {
            firstPressed.set(true)
        } else if (keyCode == keyCode2) {
            secondPressed.set(true)
        }
        if (bothPressed.value) {
            action()
        }
    }

    fun notifyKeyReleased(keyCode: KeyCode) {
        if (keyCode == keyCode1) {
            firstPressed.set(false)
        } else if (keyCode == keyCode2) {
            secondPressed.set(false)
        }
    }
}
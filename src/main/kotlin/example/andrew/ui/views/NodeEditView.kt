package example.andrew.ui.views

import example.andrew.annotations.IgnoredField
import example.andrew.annotations.FieldName
import example.andrew.annotations.IgnoreTargets
import example.andrew.sdk.OotpispApp
import example.andrew.sdk.trees.AbstractBinaryNode
import example.andrew.sdk.views.AbstractInput
import example.andrew.sdk.views.AbstractTreeView
import example.andrew.ui.extensions.*
import example.andrew.ui.styles.NodeEditStyle
import javafx.geometry.Orientation
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import tornadofx.*
import kotlin.reflect.KVisibility
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.memberProperties

class NodeEditView(tree: AbstractTreeView,
                   node: AbstractBinaryNode<Comparable<Any>, Any>,
                   callback: (valueField: AbstractInput,
                              success: () -> Unit,
                              error: (err: String) -> Unit) -> Unit) : View("Node edit") {

    lateinit var labErr: Label

    init {
        importStylesheet(NodeEditStyle::class)
    }

    override val root = vbox {
        addClass(NodeEditStyle.neMainPane)

        for (p in node::class.memberProperties) {
            val ignoreAnnot = p.findAnnotation<IgnoredField>()
            if (ignoreAnnot == null ||
                   (ignoreAnnot.target != IgnoreTargets.VIEW &&
                    ignoreAnnot.target != IgnoreTargets.ALL)) {
                if (p.visibility != KVisibility.PRIVATE) {
                    label("${p.findAnnotation<FieldName>()?.name ?: p.name}:")
                    val fieldValue = p.getter.call(node)
                    if (fieldValue != null) {
                        val type = fieldValue::class
                        val inputFactory = (app as OotpispApp).availableInputFactories.find {
                            it.type == type
                        } ?: AnyInputFactory()
                        val input = inputFactory.createInput(false)
                        (input as AbstractInput).setValue(fieldValue)
                        add(input)
                    } else {
                        val input = AnyInput() as AbstractInput
                        input.setValue("NULL")
                        add(input as Node)
                    }
                }
            }
        }

        separator(Orientation.HORIZONTAL)

        label("Key:")
        val keyInput = tree.keyFactory.createInput(editable = false)
        (keyInput as AbstractInput).setValue(node.key)
        add(keyInput)

        label("Value:")
        val valueInput = tree.valueFactory.createInput(editable = true)
        valueInput.apply {
            VBox.setVgrow(this, Priority.ALWAYS)
        }
        (valueInput as AbstractInput).setValue(node.value)
        add(valueInput)
        labErr = label {
            addClass(NodeEditStyle.labError)
            isVisible = false
        }
        anchorpane {
            VBox.setVgrow(this, Priority.ALWAYS)
            hbox {
                addClass(NodeEditStyle.butDialogContainer)
                anchorpaneConstraints {
                    bottomAnchor = 0.0
                    leftAnchor = 0.0
                    rightAnchor = 0.0
                }
                button("Cancel") {
                    setOnMouseClicked {
                        currentStage?.close()
                    }
                }
                button("Save") {
                    setOnMouseClicked {
                        callback((valueInput as AbstractInput),
                                /*success*/ {
                                    labErr.text = ""
                                    labErr.isVisible = false
                                    currentStage?.close()
                                }, /*error*/{
                                    labErr.text = it
                                    labErr.isVisible = true
                                })
                    }
                }
            }
        }
    }
}

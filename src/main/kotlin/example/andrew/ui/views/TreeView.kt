package example.andrew.ui.views

import example.andrew.sdk.trees.AbstractBinaryNode
import example.andrew.sdk.trees.AbstractBinaryTree
import example.andrew.sdk.views.AbstractTreeView
import example.andrew.sdk.views.AbstractTreeViewFactory
import example.andrew.sdk.views.InputFactory
import example.andrew.sdk.OotpispApp
import example.andrew.sdk.trees.AbstractBinaryTreeFactory
import example.andrew.ui.styles.NodeStyle
import example.andrew.ui.styles.TreeStyle
import javafx.scene.layout.Pane
import javafx.scene.shape.Line
import tornadofx.*

class TreeViewFactory: AbstractTreeViewFactory {

    override fun createTreeView(treeFactory: AbstractBinaryTreeFactory,
                                keyFactory: InputFactory,
                                valueFactory: InputFactory) =
            TreeView(treeFactory, keyFactory, valueFactory)
}

class TreeView(override val treeFactory: AbstractBinaryTreeFactory,
               override val keyFactory: InputFactory,
               override val valueFactory: InputFactory): View("Tree View"), AbstractTreeView {

    override var tree: AbstractBinaryTree<Comparable<Any>, Any>? = null

    private lateinit var mainPane: Pane
    private lateinit var edgePane: Pane

    init {
        importStylesheet(TreeStyle::class)
    }

    override val root = scrollpane {
        id = "Tree View"
        flowpane {
            addClass(TreeStyle.treeBack)
            pane {
                addClass(TreeStyle.treePane)
                edgePane = pane {
                    addClass(TreeStyle.edgePane)
                }
                mainPane = pane {
                    addClass(TreeStyle.edgePane)
                }
            }
        }
    }

    override fun repaint() {
        edgePane.children.clear()
        mainPane.children.clear()
        if (tree == null) {
            return
        }
        mainPane.prefWidth = getMaxWidth(); mainPane.prefHeight = getMaxHeight()
        edgePane.prefWidth = getMaxWidth(); edgePane.prefHeight = getMaxHeight()
        tree!!.getRoot()?.repaintRec(1)
    }

    private fun AbstractBinaryNode<Comparable<Any>, Any>.repaintRec(id: Int): Pane {
        val view = createView(id)
        if (getLeft() != null) {
            val left = getLeft()!!.repaintRec(id * 2)
            view drawLinkTo left
        }
        if (getRight() != null) {
            val right = getRight()!!.repaintRec((id * 2) + 1)
            view drawLinkTo right
        }
        return view
    }

    private infix fun Pane.drawLinkTo(another: Pane) {
        val line = Line(layoutX + (NodeStyle.nodeWidth / 2),
                layoutY + (NodeStyle.nodeHeight / 2),
                another.layoutX + (NodeStyle.nodeWidth / 2),
                another.layoutY + (NodeStyle.nodeHeight / 2))
        line.addClass(TreeStyle.link)
        edgePane.children.add(line)
    }

    private fun AbstractBinaryNode<Comparable<Any>, Any>.createView(id: Int): Pane {
        val view = (app as OotpispApp).nodeViewFactory.createNodeView(this)
        view.root.layoutX = getLevelStartGap(tree!! levelOf this) +
                getLevelGap(tree!! levelOf this) *
                (id - getNodeCountInLevel(tree!! levelOf this)) -
                (NodeStyle.nodeWidth / 2)
        view.root.layoutY = (NodeStyle.nodeHeight + TreeStyle.vGap) * (tree!! levelOf this)
        mainPane.add(view.root)
        return view.root
    }

    private fun getLevelGap(level: Int): Double {
        if (level + 1 == tree!!.getHeight()) {
            return NodeStyle.nodeWidth + TreeStyle.hGap
        }
        return getLevelGap(level + 1) * 2
    }

    private fun getLevelStartGap(level: Int): Double {
        return getMaxWidth() / (getNodeCountInLevel(level) * 2) - getLevelGapFault(level)
    }

    private fun getLevelGapFault(level: Int): Double {
        if (level == 0) {
            return 0.0
        }
        return TreeStyle.hGap / 4 + getLevelGapFault(level - 1) / 2
    }

    private fun getMaxWidth(): Double {
        return getMaxNodeCountInLevel() * NodeStyle.nodeWidth + (getMaxNodeCountInLevel() - 1) * TreeStyle.hGap
    }

    private fun getMaxHeight(): Double {
        return tree!!.getHeight() * NodeStyle.nodeHeight + (tree!!.getHeight() - 1) * TreeStyle.vGap
    }

    private fun getNodeCountInLevel(level: Int): Int {
        return 2 exp level
    }

    private fun getMaxNodeCountInLevel(): Int {
        return 2 exp (tree!!.getHeight() - 1)
    }

    private infix fun Int.exp(num: Int): Int {
        return Math.pow(this.toDouble(), num.toDouble()).toInt()
    }
}

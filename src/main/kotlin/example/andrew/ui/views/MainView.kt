package example.andrew.ui.views

import example.andrew.sdk.OotpispApp
import example.andrew.sdk.Tool
import example.andrew.ui.styles.MainStyle
import example.andrew.ui.styles.TabPaneStyle
import javafx.geometry.Orientation
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox
import tornadofx.*
import javafx.scene.effect.GaussianBlur
import java.net.URI

class MainView: View("Main View") {

    lateinit var stackPane: StackPane
    lateinit var toolsBox: VBox
    val editor = Editor(app as OotpispApp)

    var blurEffect = GaussianBlur(0.0)
    private val blurProperty = blurEffect.toProperty()

    init {
        importStylesheet(MainStyle::class)
        importStylesheet(TabPaneStyle::class)
    }

    fun addTool(tool: Tool) {
        toolsBox.children.add(button {
            disableProperty().bind(tool.disableProperty)
            style {
                backgroundImage += URI.create(tool.icoPath)
            }
            tooltip {
                text = tool.tooltip
            }
            setOnMouseClicked {
                tool.action(this)
            }
        })
    }

    fun addSeparator() {
        toolsBox.children.add(separator {
            orientation = Orientation.HORIZONTAL
        })
    }

    override val root = stackpane {
        stackPane = this
        borderpane {
            effectProperty().bind(blurProperty)
            left {
                toolbar {
                    addClass(MainStyle.controlBar)
                    toolsBox = vbox {
                        orientation = Orientation.VERTICAL
                        spacing = 10.0
                        /*button {
                            addClass(MainStyle.newTabButton)
                            VBox.setMargin(this, insets(0, 0, 30, 0))
                            tooltip {
                                text = "Add new tree"
                            }
                            setOnMouseClicked {
                                (app as OotpispApp).editor.controller.onAddTreeClick()
                                // Controller.onAddTreeClick(stackPane, tabPane)
                            }
                        }
                        button {
                            addClass(MainStyle.buttonAdd)
                            disableProperty().bind(Controller.treeTabProperty)
                            tooltip {
                                text = "Add new node"
                            }
                            setOnMouseClicked {
                                Controller.onAddNodeClick()
                            }
                        }
                        button {
                            addClass(MainStyle.buttonRemove)
                            disableProperty().bind(Controller.treeTabProperty)
                            tooltip {
                                text = "Remove node by key"
                            }
                            setOnMouseClicked {
                                Controller.onRemoveNodeClick()
                            }
                        }
                        button {
                            addClass(MainStyle.buttonFind)
                            disableProperty().bind(Controller.treeTabProperty)
                            tooltip {
                                text = "Find node by key"
                            }
                            setOnMouseClicked {
                                Controller.onFindNodeClick()
                            }
                        }
                        button {
                            addClass(MainStyle.buttonClear)
                            disableProperty().bind(Controller.treeTabProperty)
                            tooltip {
                                text = "Clear all tree"
                            }
                            setOnMouseClicked {
                                Controller.onClearTreeClick()
                            }
                        }
                        button {
                            addClass(MainStyle.buttonSave)
                            disableProperty().bind(Controller.treeTabProperty)
                            tooltip {
                                text = "Save tree"
                            }
                            setOnMouseClicked {
                                Controller.onSaveTreeClick()
                            }
                        }
                        separator(Orientation.HORIZONTAL)
                        button {
                            addClass(MainStyle.buttonLoad)
                            tooltip {
                                text = "Load tree"
                            }
                            setOnMouseClicked {
                                Controller.onLoadTreeClick(tabPane)
                            }
                        }
                        separator(Orientation.HORIZONTAL)
                        button {
                            addClass(MainStyle.buttonFirstTest)
                            tooltip {
                                text = "Test with string values"
                            }
                            setOnMouseClicked {
                                Controller.firstTest(tabPane)
                            }
                        }
                        button {
                            addClass(MainStyle.buttonSecondTest)
                            tooltip {
                                text = "Test with string's array values"
                            }
                            setOnMouseClicked {
                                Controller.secondTest(tabPane)
                            }
                        }
                        button {
                            addClass(MainStyle.buttonThirdTest)
                            tooltip {
                                text = "Test with image values"
                            }
                            setOnMouseClicked {
                                Controller.thirdTest(tabPane)
                            }
                        }*/
                    }
                }
            }
            center = editor.root
        }
    }
}
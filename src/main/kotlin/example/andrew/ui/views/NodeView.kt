package example.andrew.ui.views

import example.andrew.sdk.trees.AbstractBinaryNode
import example.andrew.sdk.views.AbstractNodeView
import example.andrew.sdk.views.AbstractNodeViewFactory
import example.andrew.sdk.OotpispApp
import example.andrew.sdk.views.AbstractInput
import example.andrew.sdk.views.InputException
import example.andrew.ui.styles.NodeStyle
import tornadofx.*

class NodeViewFactory: AbstractNodeViewFactory {

    override fun createNodeView(node: AbstractBinaryNode<Comparable<Any>, Any>) = NodeView(node)
}

class NodeView(private val node: AbstractBinaryNode<Comparable<Any>, Any>)
            : View("Node View"), AbstractNodeView {

    init {
        importStylesheet(NodeStyle::class)
    }

    override val root = flowpane {
        addClass(NodeStyle.nodePane)
        label(node.key.toString())
        setOnMouseClicked {
            onClickAction()
        }
    }

    override fun onClickAction() {
        NodeEditView((app as OotpispApp).editor.activeTreeView, node, ::onSaveNodeClick)
                .apply {
                    openModal(block = true)
                }
    }

    private fun onSaveNodeClick(valueField: AbstractInput,
                                success: () -> Unit,
                                error: (err: String) -> Unit) {
        val value = try {
            valueField.getValue()
        } catch (exc: InputException) {
            error("Value ${exc.error}")
            null
        }
        if (value != null) {
            node.value = value
            success()
        }
    }
}

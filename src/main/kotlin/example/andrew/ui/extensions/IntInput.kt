package example.andrew.ui.extensions

import example.andrew.sdk.views.AbstractInput
import example.andrew.sdk.views.InputException
import example.andrew.ui.extensions.styles.InputStyle
import javafx.scene.control.TextField
import tornadofx.*

class IntInput : TextField(), AbstractInput {

    init {
        importStylesheet(InputStyle::class)
        addClass(InputStyle.inputField)
        promptText = "Input number"
    }

    override fun getValue(): Any {
        if (text.isEmpty()) {
            addClass(InputStyle.inputError)
            throw InputException("input is empty")
        }
        try {
            val result = text.toInt()
            removeClass(InputStyle.inputError)
            return result
        } catch (exc: NumberFormatException) {
            addClass(InputStyle.inputError)
            throw InputException("input does not match the required")
        }
    }

    override fun setValue(value: Any) {
        if (value is Int) {
            text = value.toString()
        } else {
            throw InputException("input does not match the required")
        }
    }
}
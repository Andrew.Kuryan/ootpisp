package example.andrew.ui.extensions

import example.andrew.sdk.views.AbstractInput
import example.andrew.sdk.views.InputException
import example.andrew.ui.extensions.styles.InputStyle
import javafx.scene.control.TextField
import tornadofx.*

class StringInput : TextField(), AbstractInput {

    init {
        importStylesheet(InputStyle::class)
        addClass(InputStyle.inputField)
        promptText = "Input string"
    }

    override fun getValue(): Any {
        if (text.isEmpty()) {
            addClass(InputStyle.inputError)
            throw InputException("input is empty")
        }
        if (text.contains("\\") || text.contains("\"")) {
            addClass(InputStyle.inputError)
            throw InputException("input does not match the required")
        } else {
            removeClass(InputStyle.inputError)
            return text
        }
    }

    override fun setValue(value: Any) {
        text = value.toString()
    }
}
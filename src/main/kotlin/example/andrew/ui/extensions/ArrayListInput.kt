package example.andrew.ui.extensions

import example.andrew.sdk.views.AbstractInput
import example.andrew.sdk.views.InputException
import example.andrew.ui.extensions.styles.InputStyle
import javafx.collections.FXCollections
import javafx.scene.control.Label
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import tornadofx.*

class ArrayListInput(editable: Boolean) : VBox(), AbstractInput {

    private val input = StringInput()
    private val strList = FXCollections.observableArrayList<Label>()

    init {
        importStylesheet(InputStyle::class)
        if (editable) {
            hbox {
                input.apply {
                    HBox.setHgrow(this, Priority.ALWAYS)
                }
                add(input)
                button("Add") {
                    addClass(InputStyle.buttonNewString)
                    isDisable = !editable
                    setOnMouseClicked {
                        onButAddClick()
                    }
                }
            }
        }
        listview<Label> {
            items = strList
        }
    }

    override fun getValue(): Any {
        if (strList.size == 0) {
            addClass(InputStyle.inputError)
            throw InputException("input is empty")
        }
        val result = arrayListOf<String>()//Array<String>(strList.size, init = {return ""})
        for (label in strList) {
            result.add(label.text)
        }
        return result
    }

    override fun setValue(value: Any) {
        if (value is ArrayList<*>) {
            strList.clear()
            for (item in value) {
                strList.add(item?.toString()?.createCell() ?: "NULL".createCell())
            }
        }
    }

    /**Controller**/

    private fun String.createCell(): Label {
        return Label(this).apply {
            contextmenu {
                item("Remove") {
                    imageview("file:./src/main/resources/drawable/window-close.png") {
                        fitWidth = 25.0
                        fitHeight = 25.0
                    }
                    action {
                        strList.remove(this@apply)
                    }
                }
            }
        }
    }

    private fun onButAddClick() {
        try {
            val str = (input as AbstractInput).getValue() as String
            strList.add(str.createCell())
            removeClass(InputStyle.inputError)
            (input as AbstractInput).setValue("")
        } catch (exc: InputException) {
        }
    }
}
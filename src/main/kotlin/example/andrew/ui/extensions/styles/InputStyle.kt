package example.andrew.ui.extensions.styles

import example.andrew.ui.values.Colors
import example.andrew.ui.values.Fonts
import example.andrew.ui.values.labelFont
import javafx.geometry.Pos
import javafx.scene.paint.Color
import tornadofx.*

class InputStyle : Stylesheet() {

    companion object {
        val inputField by cssclass()
        val inputError by cssclass()

        val imageBack by cssclass()
        val imageInput by cssclass()

        val buttonNewString by cssclass()
    }

    init {
        val inputHeight = 30
        inputField and textField {
            prefHeight = inputHeight.px
            backgroundColor += Colors.mainBorderColor
            backgroundRadius += box(0.px)
            borderRadius += box(0.px)
            labelFont(Fonts.tfInputFont)
        }
        inputError {
            borderColor += box(c(255, 0, 0))
        }

        imageBack {
            padding = box(10.px)
            alignment = Pos.CENTER
        }

        buttonNewString and button {
            prefHeight = inputHeight.px
            backgroundColor += Colors.mainBorderColor
            backgroundRadius += box(0.px)
            borderRadius += box(0.px)
            labelFont(Fonts.simpleButtonFont)
        }
        contextMenu {
            backgroundColor += Colors.mainBorderColor
            textFill = Color.WHITE
            menuItem and hover {
                backgroundColor += Color.POWDERBLUE
                textFill = Color.BLACK
            }
            menuItem and focused {
                backgroundColor += Color.POWDERBLUE
                textFill = Color.BLACK
            }
        }
    }
}
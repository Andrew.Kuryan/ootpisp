package example.andrew.ui.extensions

import example.andrew.sdk.views.AbstractInput
import example.andrew.sdk.views.InputException
import example.andrew.ui.extensions.styles.InputStyle
import javafx.scene.control.TextField
import tornadofx.*

class AnyInput : TextField(), AbstractInput {

    init {
        importStylesheet(InputStyle::class)
        addClass(InputStyle.inputField)
        promptText = "Input object"
        this.isEditable = false
    }

    override fun getValue(): Any {
        if (text.isNotEmpty()) {
            val result = text
            removeClass(InputStyle.inputError)
            return result
        } else {
            addClass(InputStyle.inputError)
            throw InputException("input is empty")
        }
    }

    override fun setValue(value: Any) {
        text = value.toString()
    }
}
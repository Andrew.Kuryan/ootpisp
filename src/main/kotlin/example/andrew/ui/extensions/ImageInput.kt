package example.andrew.ui.extensions

import example.andrew.sdk.views.AbstractInput
import example.andrew.sdk.views.InputException
import example.andrew.ui.extensions.styles.InputStyle
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.FlowPane
import javafx.stage.FileChooser
import tornadofx.*

class ImageInput: FlowPane(), AbstractInput {

    var editable = true
    private var isEmpty = true

    private val imageView: ImageView
    private val image = Image("file:./src/main/resources/drawable/insert-image.png")
    private val imageProperty = image.toProperty()

    init {
        importStylesheet(InputStyle::class)
        addClass(InputStyle.imageBack)
        imageView = imageview {
            imageProperty().bind(imageProperty)
            addClass(InputStyle.imageInput)
            fitWidth = 180.0; fitHeight = 180.0
            isPreserveRatio = true
            setOnMouseClicked {
                onSelectImageClick()
            }
        }
    }

    override fun getValue(): Any {
        if (isEmpty || imageView.image == null) {
            addClass(InputStyle.inputError)
            throw InputException("input is empty")
        }
        return imageView.image
    }

    override fun setValue(value: Any) {
        if (value is Image) {
            imageProperty.set(value)
            isEmpty = false
        }
    }

    /**Controller**/

    private fun onSelectImageClick() {
        if (editable) {
            val files = chooseFile("Choose image",
                    arrayOf(FileChooser.ExtensionFilter("Images", "*.png")),
                    FileChooserMode.Single,
                    scene.window)

            if (files.isNotEmpty()) {
                removeClass(InputStyle.inputError)
                imageProperty.set(Image("file:${files.first().canonicalPath}"))
                isEmpty = false
            }
        }
    }
}
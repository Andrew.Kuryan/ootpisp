package example.andrew.ui.extensions

import example.andrew.sdk.views.InputFactory
import javafx.scene.Node
import javafx.scene.image.Image

class AnyInputFactory : InputFactory {

    override val type = Any::class

    override fun createInput(editable: Boolean) = AnyInput()
}

class IntInputFactory : InputFactory {

    override val type = Int::class

    override fun createInput(editable: Boolean): Node {
        val input = IntInput()
        input.isEditable = editable
        return input
    }
}

class StringInputFactory : InputFactory {

    override val type = String::class

    override fun createInput(editable: Boolean): Node {
        val input = StringInput()
        input.isEditable = editable
        return input
    }
}

class ArrayListInputFactory : InputFactory {

    override val type = ArrayList::class

    override fun createInput(editable: Boolean): Node {
        return ArrayListInput(editable)
    }
}

class ImageInputFactory : InputFactory {

    override val type= Image::class

    override fun createInput(editable: Boolean): Node {
        val input = ImageInput()
        input.editable = editable
        return input
    }
}
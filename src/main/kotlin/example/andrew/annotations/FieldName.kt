package example.andrew.annotations

@Target(AnnotationTarget.PROPERTY)
annotation class FieldName(val name: String)
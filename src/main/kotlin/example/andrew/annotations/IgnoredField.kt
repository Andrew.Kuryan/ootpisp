package example.andrew.annotations

enum class IgnoreTargets {
    VIEW, SERIALIZE, ALL
}

@Target(AnnotationTarget.PROPERTY)
annotation class IgnoredField(val target: IgnoreTargets)